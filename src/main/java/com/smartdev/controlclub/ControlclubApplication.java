package com.smartdev.controlclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlclubApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlclubApplication.class, args);
	}

}

