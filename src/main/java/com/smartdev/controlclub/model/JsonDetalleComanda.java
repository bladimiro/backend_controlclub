/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author bladimir
 */
public class JsonDetalleComanda {
    private Integer detalleComandaId;
    private Integer comandaMeseroId;
    private Integer pedidoId;
    private Integer nroPedido;
    private Integer estadoPedidoId;
    private Integer tipoPagoId;
    private Date fechaRegistro;
    private List<JsonDetallePedido> datosPedido;

    public Integer getDetalleComandaId() {
        return detalleComandaId;
    }

    public void setDetalleComandaId(Integer detalleComandaId) {
        this.detalleComandaId = detalleComandaId;
    }

    public Integer getComandaMeseroId() {
        return comandaMeseroId;
    }

    public void setComandaMeseroId(Integer comandaMeseroId) {
        this.comandaMeseroId = comandaMeseroId;
    }

    public Integer getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Integer pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Integer getNroPedido() {
        return nroPedido;
    }

    public void setNroPedido(Integer nroPedido) {
        this.nroPedido = nroPedido;
    }

    public Integer getEstadoPedidoId() {
        return estadoPedidoId;
    }

    public void setEstadoPedidoId(Integer estadoPedidoId) {
        this.estadoPedidoId = estadoPedidoId;
    }

    public Integer getTipoPagoId() {
        return tipoPagoId;
    }

    public void setTipoPagoId(Integer tipoPagoId) {
        this.tipoPagoId = tipoPagoId;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public List<JsonDetallePedido> getDatosPedido() {
        return datosPedido;
    }

    public void setDatosPedido(List<JsonDetallePedido> datosPedido) {
        this.datosPedido = datosPedido;
    }
    public JsonDetalleComanda() {
    }

    public JsonDetalleComanda(Integer detalleComandaId, Integer comandaMeseroId, Integer pedidoId, Integer nroPedido) {
        this.detalleComandaId = detalleComandaId;
        this.comandaMeseroId = comandaMeseroId;
        this.pedidoId = pedidoId;
        this.nroPedido = nroPedido;
    }
    
}
