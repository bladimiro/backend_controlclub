/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.List;

/**
 *
 * @author bladimir
 */
public class JsonDetallePedido {
    private Integer detallePedidoId;
    private Integer cantidad;
    private double precioUnitario;
    private Integer pedidoId;
    private Integer subproductoId;
    private String nombreSubproducto;
    private Integer usuarioId;
    private List<JsonPedidoDama> pedidosDama;
    private String damasConcatenadas;

    public Integer getDetallePedidoId() {
        return detallePedidoId;
    }

    public void setDetallePedidoId(Integer detallePedidoId) {
        this.detallePedidoId = detallePedidoId;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
    
    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Integer getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Integer pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Integer getSubproductoId() {
        return subproductoId;
    }

    public void setSubproductoId(Integer subproductoId) {
        this.subproductoId = subproductoId;
    }

    public String getNombreSubproducto() {
        return nombreSubproducto;
    }

    public void setNombreSubproducto(String nombreSubproducto) {
        this.nombreSubproducto = nombreSubproducto;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }    

    public List<JsonPedidoDama> getPedidosDama() {
        return pedidosDama;
    }

    public void setPedidosDama(List<JsonPedidoDama> pedidosDama) {
        this.pedidosDama = pedidosDama;
    }

    public String getDamasConcatenadas() {
        return damasConcatenadas;
    }

    public void setDamasConcatenadas(String damasConcatenadas) {
        this.damasConcatenadas = damasConcatenadas;
    }

    public JsonDetallePedido() {
    }

    public JsonDetallePedido(Integer detallePedidoId, Integer cantidad, double precioUnitario, String nombreSubproducto) {
        this.detallePedidoId = detallePedidoId;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.nombreSubproducto = nombreSubproducto;
    }
    

    public JsonDetallePedido(Integer detallePedidoId, Integer cantidad, double precioUnitario, Integer pedidoId, Integer subproductoId, String nombreSubproducto, Integer usuarioId) {
        this.detallePedidoId = detallePedidoId;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.pedidoId = pedidoId;
        this.subproductoId = subproductoId;
        this.nombreSubproducto = nombreSubproducto;
        this.usuarioId = usuarioId;
    }
    
    public JsonDetallePedido(Integer detallePedidoId, Integer cantidad, double precioUnitario, Integer pedidoId, Integer subproductoId, String nombreSubproducto, Integer usuarioId, String damasConcatenadas) {
        this.detallePedidoId = detallePedidoId;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.pedidoId = pedidoId;
        this.subproductoId = subproductoId;
        this.nombreSubproducto = nombreSubproducto;
        this.usuarioId = usuarioId;
        this.damasConcatenadas = damasConcatenadas;
    }
    
    public JsonDetallePedido(Integer detallePedidoId, Integer cantidad, double precioUnitario, Integer pedidoId, Integer subproductoId, String nombreSubproducto, Integer usuarioId, String damasConcatenadas, List<JsonPedidoDama> pedidosDama) {
        this.detallePedidoId = detallePedidoId;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.pedidoId = pedidoId;
        this.subproductoId = subproductoId;
        this.nombreSubproducto = nombreSubproducto;
        this.usuarioId = usuarioId;
        this.damasConcatenadas = damasConcatenadas;
        this.pedidosDama = pedidosDama;
    }
}
