/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.List;

/**
 *
 * @author bladimir
 */
public class JsonOpciones {
    private Integer opcionId;
    private String nombre;
    private String enlace;
    private Integer orden;
    private List<JsonOpciones> subopciones;

    public Integer getOpcionId() {
        return opcionId;
    }

    public void setOpcionId(Integer opcionId) {
        this.opcionId = opcionId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public List<JsonOpciones> getSubopciones() {
        return subopciones;
    }

    public void setSubopciones(List<JsonOpciones> subopciones) {
        this.subopciones = subopciones;
    }

    public JsonOpciones() {
    }

    public JsonOpciones(Integer opcionId, String nombre, String enlace, Integer orden) {
        this.opcionId = opcionId;
        this.nombre = nombre;
        this.enlace = enlace;
        this.orden = orden;
    }
    
}
