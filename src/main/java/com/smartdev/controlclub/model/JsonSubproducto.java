/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.List;

/**
 *
 * @author bladimir
 */
public class JsonSubproducto {
    private Integer subproductoId;
    private Integer productoId;
    private String nombre;
    private Integer tipoId;
    private Double precio;
    private Double equivalencia;
    private Integer cantidad;
    private Integer codigo;
    private Integer cantidadChicas;
    private Integer tipoSolicitudId;
    private Integer damaId;
    private List<JsonPedidoDama> pedidoDamas;
    private Double comision;

    public Integer getSubproductoId() {
        return subproductoId;
    }

    public void setSubproductoId(Integer subproductoId) {
        this.subproductoId = subproductoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    public Integer getTipoId() {
        return tipoId;
    }

    public void setTipoId(Integer tipoId) {
        this.tipoId = tipoId;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getEquivalencia() {
        return equivalencia;
    }

    public void setEquivalencia(Double equivalencia) {
        this.equivalencia = equivalencia;
    }

    public Integer getCantidadChicas() {
        return cantidadChicas;
    }

    public void setCantidadChicas(Integer cantidadChicas) {
        this.cantidadChicas = cantidadChicas;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public JsonSubproducto() {
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getTipoSolicitudId() {
        return tipoSolicitudId;
    }

    public void setTipoSolicitudId(Integer tipoSolicitudId) {
        this.tipoSolicitudId = tipoSolicitudId;
    }

    public Integer getDamaId() {
        return damaId;
    }

    public void setDamaId(Integer damaId) {
        this.damaId = damaId;
    }

    public List<JsonPedidoDama> getPedidoDamas() {
        return pedidoDamas;
    }

    public void setPedidoDamas(List<JsonPedidoDama> pedidoDamas) {
        this.pedidoDamas = pedidoDamas;
    }

    public Double getComision() {
        return comision;
    }

    public void setComision(Double comision) {
        this.comision = comision;
    }

    public JsonSubproducto(Integer subproductoId, Integer productoId, String nombre, Integer tipoId, Double precio) {
        this.subproductoId = subproductoId;
        this.productoId = productoId;
        this.tipoId = tipoId;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = 0;
    }
    
    public JsonSubproducto(Integer subproductoId, Integer productoId, String nombre, Integer tipoId, Double precio, Double equivalencia) {
        this.subproductoId = subproductoId;
        this.productoId = productoId;
        this.tipoId = tipoId;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = 0;
        this.equivalencia = equivalencia;
    }

    public JsonSubproducto(Integer subproductoId, Integer productoId, String nombre, Integer tipoId, Double precio, Double equivalencia, Integer cantidadChicas, Integer tipoSolicitudId) {
        this.subproductoId = subproductoId;
        this.productoId = productoId;
        this.nombre = nombre;
        this.tipoId = tipoId;
        this.precio = precio;
        this.equivalencia = equivalencia;
        this.cantidadChicas = cantidadChicas;
        this.tipoSolicitudId = tipoSolicitudId;
    }
    
    public JsonSubproducto(Integer subproductoId, Integer productoId, String nombre, Integer tipoId, Double precio, Double equivalencia, Integer cantidadChicas, Integer tipoSolicitudId, Double comision) {
        this.subproductoId = subproductoId;
        this.productoId = productoId;
        this.nombre = nombre;
        this.tipoId = tipoId;
        this.precio = precio;
        this.equivalencia = equivalencia;
        this.cantidadChicas = cantidadChicas;
        this.tipoSolicitudId = tipoSolicitudId;
        this.comision = comision;
    }
}
