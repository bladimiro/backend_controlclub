/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

/**
 *
 * @author bladimir
 */
public class JsonProducto {
    private Integer productoId;
    private String nombre;
    private Double precio;
    private Integer usuarioId;
    private Integer cantidadVasos;

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Integer getCantidadVasos() {
        return cantidadVasos;
    }

    public void setCantidadVasos(Integer cantidadVasos) {
        this.cantidadVasos = cantidadVasos;
    }
    
}
