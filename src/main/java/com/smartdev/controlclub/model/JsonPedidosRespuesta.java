/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import com.smartdev.controlclub.entities.DetallePedido;
import java.util.Date;
import java.util.List;

/**
 *
 * @author bladimir
 */
public class JsonPedidosRespuesta {
    private Integer pedidoId;
    private Integer clienteId;
    private String nombreCliente;
    private Integer mesaId;
    private String numeroMesa; 
    private Integer usuarioId;
    private String username;
    private Integer estadoPedidoId;
    private Integer numeroPedido;
    private double montoEntregado;
    private double montoTotal;
    private Integer tipoPagoId;
    private List<JsonDetallePedido> detalle;
    private String observacion;
    private Date fechaRegistro;

    public Integer getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Integer pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Integer getMesaId() {
        return mesaId;
    }

    public void setMesaId(Integer mesaId) {
        this.mesaId = mesaId;
    }

    public String getNumeroMesa() {
        return numeroMesa;
    }

    public void setNumeroMesa(String numeroMesa) {
        this.numeroMesa = numeroMesa;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getEstadoPedidoId() {
        return estadoPedidoId;
    }

    public void setEstadoPedidoId(Integer estadoPedidoId) {
        this.estadoPedidoId = estadoPedidoId;
    }

    public Integer getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public double getMontoEntregado() {
        return montoEntregado;
    }

    public void setMontoEntregado(double montoEntregado) {
        this.montoEntregado = montoEntregado;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getTipoPagoId() {
        return tipoPagoId;
    }

    public void setTipoPagoId(Integer tipoPagoId) {
        this.tipoPagoId = tipoPagoId;
    }

    public List<JsonDetallePedido> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<JsonDetallePedido> detalle) {
        this.detalle = detalle;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public JsonPedidosRespuesta() {
    }
    
}
