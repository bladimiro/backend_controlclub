/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.Date;

/**
 *
 * @author bladimir
 */
public class JsonPedidoDama {
    private Integer pedidoDamaId;
    private Integer damaId;
    private Integer detallePedidoId;
    private String nombreDama;
    private Double comision;
    private Integer numeroPedido;
    private String nombreProducto;
    private Integer cantidad;
    private Date fechaRegistro;

    public Integer getPedidoDamaId() {
        return pedidoDamaId;
    }

    public void setPedidoDamaId(Integer pedidoDamaId) {
        this.pedidoDamaId = pedidoDamaId;
    }

    public Integer getDamaId() {
        return damaId;
    }

    public void setDamaId(Integer damaId) {
        this.damaId = damaId;
    }

    public Integer getDetallePedidoId() {
        return detallePedidoId;
    }

    public void setDetallePedidoId(Integer detallePedidoId) {
        this.detallePedidoId = detallePedidoId;
    }

    public String getNombreDama() {
        return nombreDama;
    }

    public void setNombreDama(String nombreDama) {
        this.nombreDama = nombreDama;
    }

    public Double getComision() {
        return comision;
    }

    public void setComision(Double comision) {
        this.comision = comision;
    }

    public Integer getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public JsonPedidoDama() {
    }
    
    public JsonPedidoDama(Integer pedidoDamaId, Integer damaId, Integer detallePedidoId, String nombreDama) {
        this.pedidoDamaId = pedidoDamaId;
        this.damaId = damaId;
        this.detallePedidoId = detallePedidoId;
        this.nombreDama = nombreDama;
    }

    public JsonPedidoDama(Integer pedidoDamaId, Integer damaId, Integer detallePedidoId, String nombreDama, Double comision) {
        this.pedidoDamaId = pedidoDamaId;
        this.damaId = damaId;
        this.detallePedidoId = detallePedidoId;
        this.nombreDama = nombreDama;
        this.comision = comision;
    }
    
}
