/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.Date;

/**
 *
 * @author bladimir
 */
public class JsonMovimiento {
    private Integer movimientoId;
    private String tipoId;
    private Double monto;
    private Date fechaMovimiento;
    private String glosa;
    private Integer gastoId;
    private Integer pedidoId;
    private Double montoIngreso;
    private Double montoEgreso;

    public Integer getMovimientoId() {
        return movimientoId;
    }

    public void setMovimientoId(Integer movimientoId) {
        this.movimientoId = movimientoId;
    }

    public String getTipoId() {
        return tipoId;
    }

    public void setTipoId(String tipoId) {
        this.tipoId = tipoId;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Date getFechaMovimiento() {
        return fechaMovimiento;
    }

    public void setFechaMovimiento(Date fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public Integer getGastoId() {
        return gastoId;
    }

    public void setGastoId(Integer gastoId) {
        this.gastoId = gastoId;
    }

    public Integer getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Integer pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Double getMontoIngreso() {
        return montoIngreso;
    }

    public void setMontoIngreso(Double montoIngreso) {
        this.montoIngreso = montoIngreso;
    }

    public Double getMontoEgreso() {
        return montoEgreso;
    }

    public void setMontoEgreso(Double montoEgreso) {
        this.montoEgreso = montoEgreso;
    }

    public JsonMovimiento() {
    }

    public JsonMovimiento(Integer movimientoId, String tipoId, Double monto, Date fechaMovimiento, String glosa) {
        this.movimientoId = movimientoId;
        this.tipoId = tipoId;
        this.monto = monto;
        this.fechaMovimiento = fechaMovimiento;
        this.glosa = glosa;
    }
    
}
