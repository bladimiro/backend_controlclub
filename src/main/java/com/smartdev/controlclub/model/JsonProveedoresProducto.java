/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.Date;

/**
 *
 * @author bladimir
 */
public class JsonProveedoresProducto {
    private Integer provProductoId;
    private Integer cantidad;
    private Date fechaCompra;
    private Double precio;
    private Integer productoId;
    private String productoNombre;
    private Integer proveedorId;
    private String proveedorNombre;
    private Integer usuarioId;

    public Integer getProvProductoId() {
        return provProductoId;
    }

    public void setProvProductoId(Integer provProductoId) {
        this.provProductoId = provProductoId;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    public String getProductoNombre() {
        return productoNombre;
    }

    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }

    public Integer getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(Integer proveedorId) {
        this.proveedorId = proveedorId;
    }

    public String getProveedorNombre() {
        return proveedorNombre;
    }

    public void setProveedorNombre(String proveedorNombre) {
        this.proveedorNombre = proveedorNombre;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public JsonProveedoresProducto() {
    }

    public JsonProveedoresProducto(Integer provProductoId, Integer cantidad, Date fechaCompra, Double precio, Integer productoId, String productoNombre, Integer proveedorId, String proveedorNombre) {
        this.provProductoId = provProductoId;
        this.cantidad = cantidad;
        this.fechaCompra = fechaCompra;
        this.precio = precio;
        this.productoId = productoId;
        this.productoNombre = productoNombre;
        this.proveedorId = proveedorId;
        this.proveedorNombre = proveedorNombre;
    }
    
}
