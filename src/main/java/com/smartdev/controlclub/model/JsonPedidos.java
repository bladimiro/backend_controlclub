/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.List;

/**
 *
 * @author bladimir
 */
public class JsonPedidos {
    private Integer pedidoId;
    private Integer clienteId;
    private String nombreCliente;
    private Integer mesaId;
    private Integer usuarioId;
    private Integer estadoPedidoId;
    private Integer numeroPedido;
    private double montoEntregado;
    private double montoTotal;
    private double montoCambio;
    private Integer tipoPagoId;
    private List<JsonSubproducto> subproductos;
    private String observacion;
    private Integer usuarioDespachaId;
    private Integer usuarioCancelaId;
    private Integer damaId;

    public Integer getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Integer pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Integer getMesaId() {
        return mesaId;
    }

    public void setMesaId(Integer mesaId) {
        this.mesaId = mesaId;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Integer getEstadoPedidoId() {
        return estadoPedidoId;
    }

    public void setEstadoPedidoId(Integer estadoPedidoId) {
        this.estadoPedidoId = estadoPedidoId;
    }

    public Integer getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public double getMontoEntregado() {
        return montoEntregado;
    }

    public void setMontoEntregado(double montoEntregado) {
        this.montoEntregado = montoEntregado;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public double getMontoCambio() {
        return montoCambio;
    }

    public void setMontoCambio(double montoCambio) {
        this.montoCambio = montoCambio;
    }

    public Integer getTipoPagoId() {
        return tipoPagoId;
    }

    public void setTipoPagoId(Integer tipoPagoId) {
        this.tipoPagoId = tipoPagoId;
    }

    public List<JsonSubproducto> getSubproductos() {
        return subproductos;
    }

    public void setSubproductos(List<JsonSubproducto> subproductos) {
        this.subproductos = subproductos;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getUsuarioDespachaId() {
        return usuarioDespachaId;
    }

    public void setUsuarioDespachaId(Integer usuarioDespachaId) {
        this.usuarioDespachaId = usuarioDespachaId;
    }

    public Integer getUsuarioCancelaId() {
        return usuarioCancelaId;
    }

    public void setUsuarioCancelaId(Integer usuarioCancelaId) {
        this.usuarioCancelaId = usuarioCancelaId;
    }

    public Integer getDamaId() {
        return damaId;
    }

    public void setDamaId(Integer damaId) {
        this.damaId = damaId;
    }
    
}
