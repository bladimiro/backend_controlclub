/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

/**
 *
 * @author bladimir
 */
public class JsonComandasMeseros {
    private Integer comadaMeseroId;
    private Integer usuarioId;
    private String username;
    private Double total;

    public Integer getComadaMeseroId() {
        return comadaMeseroId;
    }

    public void setComadaMeseroId(Integer comadaMeseroId) {
        this.comadaMeseroId = comadaMeseroId;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
    
}
