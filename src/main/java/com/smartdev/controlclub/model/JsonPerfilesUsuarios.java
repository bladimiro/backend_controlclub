/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

/**
 *
 * @author bladimir
 */
public class JsonPerfilesUsuarios {
    private Integer perfilUsuarioId;
    private Integer usuarioId;
    private Integer perfilId;
    private String perfNombre;

    public Integer getPerfilUsuarioId() {
        return perfilUsuarioId;
    }

    public void setPerfilUsuarioId(Integer perfilUsuarioId) {
        this.perfilUsuarioId = perfilUsuarioId;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Integer getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(Integer perfilId) {
        this.perfilId = perfilId;
    }

    public String getPerfNombre() {
        return perfNombre;
    }

    public void setPerfNombre(String perfNombre) {
        this.perfNombre = perfNombre;
    }

    public JsonPerfilesUsuarios() {
    }

    public JsonPerfilesUsuarios(Integer perfilUsuarioId, Integer usuarioId, Integer perfilId, String perfNombre) {
        this.perfilUsuarioId = perfilUsuarioId;
        this.usuarioId = usuarioId;
        this.perfilId = perfilId;
        this.perfNombre = perfNombre;
    }
    
}
