/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

/**
 *
 * @author bladimir
 */
public class JsonMesa {
    private Integer mesaId;
    private Integer salaId;
    private String numero;

    public Integer getMesaId() {
        return mesaId;
    }

    public void setMesaId(Integer mesaId) {
        this.mesaId = mesaId;
    }

    public Integer getSalaId() {
        return salaId;
    }

    public void setSalaId(Integer salaId) {
        this.salaId = salaId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public JsonMesa() {
    }

    public JsonMesa(Integer mesaId, Integer salaId, String numero) {
        this.mesaId = mesaId;
        this.salaId = salaId;
        this.numero = numero;
    }
    
    
}
