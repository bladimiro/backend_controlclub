/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

/**
 *
 * @author bladimir
 */
public class JsonSala {
    private Integer salaId;
    private Integer empresaId;
    private String nombreSala;
    private String turno;

    public Integer getSalaId() {
        return salaId;
    }

    public void setSalaId(Integer salaId) {
        this.salaId = salaId;
    }

    public Integer getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Integer empresaId) {
        this.empresaId = empresaId;
    }

    public String getNombreSala() {
        return nombreSala;
    }

    public void setNombreSala(String nombreSala) {
        this.nombreSala = nombreSala;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public JsonSala(Integer salaId, Integer empresaId, String nombreSala, String turno) {
        this.salaId = salaId;
        this.empresaId = empresaId;
        this.nombreSala = nombreSala;
        this.turno = turno;
    }
    
}
