/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

/**
 *
 * @author bladimir
 */
public class JsonDetalleInventario {
    private Integer detalleInventarioId;
    private Double cantidadDisponible;
    private Double cantidadUsada;
    private Integer inventarioId;
    private Integer productoId;
    private String nombreProducto;

    public Integer getDetalleInventarioId() {
        return detalleInventarioId;
    }

    public void setDetalleInventarioId(Integer detalleInventarioId) {
        this.detalleInventarioId = detalleInventarioId;
    }

    public Double getCantidadDisponible() {
        return cantidadDisponible;
    }

    public void setCantidadDisponible(Double cantidadDisponible) {
        this.cantidadDisponible = cantidadDisponible;
    }

    public Double getCantidadUsada() {
        return cantidadUsada;
    }

    public void setCantidadUsada(Double cantidadUsada) {
        this.cantidadUsada = cantidadUsada;
    }

    public Integer getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(Integer inventarioId) {
        this.inventarioId = inventarioId;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public JsonDetalleInventario() {
    }

    public JsonDetalleInventario(Integer detalleInventarioId, Double cantidadDisponible, Double cantidadUsada, Integer inventarioId, Integer productoId, String nombreProducto) {
        this.detalleInventarioId = detalleInventarioId;
        this.cantidadDisponible = cantidadDisponible;
        this.cantidadUsada = cantidadUsada;
        this.inventarioId = inventarioId;
        this.productoId = productoId;
        this.nombreProducto = nombreProducto;
    }
    
}
