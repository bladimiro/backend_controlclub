/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.Date;

/**
 *
 * @author bladimir
 */
public class JsonGasto {
    private Integer gastoId;
    private Integer tipoGastoId;
    private String tipoGastoDesc;
    private Integer usuarioId;
    private String descripcion;
    private Date fechaGasto;
    private Double monto;

    public Integer getGastoId() {
        return gastoId;
    }

    public void setGastoId(Integer gastoId) {
        this.gastoId = gastoId;
    }

    public Integer getTipoGastoId() {
        return tipoGastoId;
    }

    public void setTipoGastoId(Integer tipoGastoId) {
        this.tipoGastoId = tipoGastoId;
    }

    public String getTipoGastoDesc() {
        return tipoGastoDesc;
    }

    public void setTipoGastoDesc(String tipoGastoDesc) {
        this.tipoGastoDesc = tipoGastoDesc;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaGasto() {
        return fechaGasto;
    }

    public void setFechaGasto(Date fechaGasto) {
        this.fechaGasto = fechaGasto;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public JsonGasto() {
    }

    public JsonGasto(Integer gastoId, Integer tipoGastoId, String tipoGastoDesc, String descripcion, Date fechaGasto, Double monto) {
        this.gastoId = gastoId;
        this.tipoGastoId = tipoGastoId;
        this.tipoGastoDesc = tipoGastoDesc;
        this.descripcion = descripcion;
        this.fechaGasto = fechaGasto;
        this.monto = monto;
    }
    
}
