/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author bladimir
 */
public class JsonInventario {
    private Integer inventarioId;
    private Integer estadoInventarioId;
    private Date fechaInicio;
    private Date fechaCierre;
    private Date fechaRegistro;
    private Integer usuarioId;
    private Integer usuarioCierreId;
    private List<JsonDetalleInventario> detalleInventario;

    public Integer getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(Integer inventarioId) {
        this.inventarioId = inventarioId;
    }

    public Integer getEstadoInventarioId() {
        return estadoInventarioId;
    }

    public void setEstadoInventarioId(Integer estadoInventarioId) {
        this.estadoInventarioId = estadoInventarioId;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Integer getUsuarioCierreId() {
        return usuarioCierreId;
    }

    public void setUsuarioCierreId(Integer usuarioCierreId) {
        this.usuarioCierreId = usuarioCierreId;
    }

    public List<JsonDetalleInventario> getDetalleInventario() {
        return detalleInventario;
    }

    public void setDetalleInventario(List<JsonDetalleInventario> detalleInventario) {
        this.detalleInventario = detalleInventario;
    }

    public JsonInventario() {
    }

    public JsonInventario(Integer inventarioId, Integer estadoInventarioId, Date fechaInicio, Date fechaCierre, Integer usuarioId, Integer usuarioCierreId) {
        this.inventarioId = inventarioId;
        this.estadoInventarioId = estadoInventarioId;
        this.fechaInicio = fechaInicio;
        this.fechaCierre = fechaCierre;
        this.usuarioId = usuarioId;
        this.usuarioCierreId = usuarioCierreId;
    }
    
}
