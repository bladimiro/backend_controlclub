/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleInventario;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class DetalleInventarioServices implements DetalleInventarioInterfaces{
    
    @Autowired
    private DetalleInventarioRepository detalleRepository;

    @Override
    public List<DetalleInventario> findByInventarioId(Integer inventarioId) {
        return detalleRepository.findByInventarioId(inventarioId);
    }

    @Override
    public Optional<DetalleInventario> findById(Integer DetalleInventarioId) {
        return detalleRepository.findById(DetalleInventarioId);
    }

    @Override
    public void insertOrUpdate(DetalleInventario detalle) {
        detalleRepository.save(detalle);
    }

    @Override
    public void delete(DetalleInventario detalle) {
        detalle.setEstadoId(ConstEstados.INACTIVO);
        detalleRepository.save(detalle);
    }

    @Override
    public Optional<DetalleInventario> findByInventarioIdProductoId(Integer inventarioId, Integer productoId) {
        return detalleRepository.findByInventarioIdProductoId(inventarioId, productoId);
    }
}
