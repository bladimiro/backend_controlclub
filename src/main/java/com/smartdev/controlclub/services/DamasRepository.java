/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Damas;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface DamasRepository extends JpaRepository<Damas, Integer>{
    
    @Query("select d from Damas d where d.estadoId=1000")
    List<Damas> findByActives();
    
    @Query("select d from Damas d where d.estadoId=1000 and d.habilitada=:habilitada")
    List<Damas> findByHabilitado(@Param("habilitada") boolean habilitada);
    
    @Query("select d from Damas d where d.estadoId=1000 and " +
            "d.habilitada=true and d.codigo = :codigo")
    Optional<Damas> findByCodigo(@Param("codigo") String codigo);
}
