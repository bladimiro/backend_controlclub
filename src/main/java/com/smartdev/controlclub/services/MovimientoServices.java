/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Movimiento;
import com.smartdev.controlclub.model.JsonMovimiento;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class MovimientoServices implements MovimientoInterfaces{

    @Autowired
    private MovimientoRepository movimientoRepository;
    
    @Override
    public List<JsonMovimiento> findByActives() {
        List<Movimiento> movimientos = movimientoRepository.findByActives();
        List<JsonMovimiento> respuesta = new ArrayList<>();
        JsonMovimiento jmovi;
        for(Movimiento movi: movimientos){
            jmovi = new JsonMovimiento();
            jmovi.setMovimientoId(movi.getMovimientoId());
            jmovi.setTipoId(movi.getTipoId());
            jmovi.setMonto(movi.getMonto());
            jmovi.setFechaMovimiento(movi.getFechaMovimiento());
            jmovi.setGlosa(movi.getGlosa());
            if(movi.getGastoId() != null)
                jmovi.setGastoId(movi.getGastoId().getGastoId());
            if(movi.getPedidoId() != null)
                jmovi.setPedidoId(movi.getPedidoId().getPedidoId());
            respuesta.add(jmovi);
        }
        return respuesta;
    }

    @Override
    public Optional<Movimiento> findById(Integer movimientoId) {
        return movimientoRepository.findById(movimientoId);
    }

    @Override
    public void insertOrUpdate(Movimiento movimiento) {
        movimientoRepository.save(movimiento);
    }

    @Override
    public void delete(Movimiento movimiento) {
        movimiento.setEstadoId(ConstEstados.INACTIVO);
        movimientoRepository.save(movimiento);
    }

    @Override
    public Optional<Movimiento> findByGastoId(Integer gastoId) {
        return movimientoRepository.findByGastoId(gastoId);
    }

    @Override
    public Optional<Movimiento> findByPedidoId(Integer pedidoId) {
        return movimientoRepository.findByPedidoId(pedidoId);
    }

    @Override
    public List<JsonMovimiento> findByFechas(Date fechaInicio, Date fechaFin) {
        List<Movimiento> movimientos = movimientoRepository.findByFechas(fechaInicio, fechaFin);
        List<JsonMovimiento> respuesta = new ArrayList<>();
        JsonMovimiento jmovi;
        for(Movimiento movi: movimientos){
            jmovi = new JsonMovimiento();
            jmovi.setMovimientoId(movi.getMovimientoId());
            jmovi.setTipoId(movi.getTipoId());
            if(movi.getTipoId().equals("I")){
                jmovi.setMontoIngreso(movi.getMonto());
                jmovi.setMontoEgreso(0D);
            } else {
                jmovi.setMontoIngreso(0D);
                jmovi.setMontoEgreso(movi.getMonto());
            }
            //jmovi.setMonto(movi.getMonto());
            jmovi.setFechaMovimiento(movi.getFechaMovimiento());
            jmovi.setGlosa(movi.getGlosa());
            if(movi.getGastoId() != null)
                jmovi.setGastoId(movi.getGastoId().getGastoId());
            if(movi.getPedidoId() != null)
                jmovi.setPedidoId(movi.getPedidoId().getPedidoId());
            respuesta.add(jmovi);
        }
        return respuesta;
    }
    
}
