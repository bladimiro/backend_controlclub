/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleInventario;
import com.smartdev.controlclub.entities.Inventario;
import com.smartdev.controlclub.entities.Proveedor;
import com.smartdev.controlclub.entities.ProveedorProducto;
import com.smartdev.controlclub.model.JsonProveedoresProducto;
import com.smartdev.controlclub.utils.ConstEstadoInventario;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class ProveedoresProductoServices implements ProveedoresProductoInterface {

    @Autowired
    private ProveedoresProductoRepository provProductoRepository;
    
    @Autowired
    private InventariosServices inventarioService;
    
    @Autowired
    private DetalleInventarioServices detalleInvenService;

    @Override
    public List<JsonProveedoresProducto> findByProductoId(Integer productoId) {
        return provProductoRepository.findByProductoId(productoId);
    }

    @Override
    public Optional<ProveedorProducto> findById(Integer provProductoId) {
        return provProductoRepository.findById(provProductoId);
    }

    @Override
    public void insertOrUpdate(ProveedorProducto proveedorProducto) {
        provProductoRepository.save(proveedorProducto);
        Double cantidad = 0D;
        Double cantidad_usada = 0D;
        DetalleInventario detalleInventario;
        
        Inventario inventario = inventarioService.findByEstadoInventario(ConstEstadoInventario.ABIERTO).get();
        Optional<DetalleInventario> detalleInvenProd = detalleInvenService.findByInventarioIdProductoId(
                                    inventario.getInventarioId(), proveedorProducto.getProductoId().getProductoId());
        if(detalleInvenProd.isPresent()){
            detalleInventario = detalleInvenProd.get();
            detalleInventario.setEstadoId(ConstEstados.HISTORICO);
            detalleInvenService.insertOrUpdate(detalleInventario);
            cantidad = detalleInventario.getCantidadDisponible();
            cantidad_usada = detalleInventario.getCantidadUsada();
        }
        cantidad += proveedorProducto.getCantidad();
        detalleInventario =  new DetalleInventario();
        detalleInventario.setInventarioId(inventario);
        detalleInventario.setProductoId(proveedorProducto.getProductoId());
        detalleInventario.setUsuarioId(proveedorProducto.getUsuarioId());
        detalleInventario.setCantidadDisponible(cantidad);
        detalleInventario.setCantidadUsada(cantidad_usada);
        detalleInventario.setFechaRegistro(new Date());
        detalleInventario.setEstadoId(ConstEstados.ACTIVO);
        detalleInvenService.insertOrUpdate(detalleInventario);
    }

    @Override
    public void delete(ProveedorProducto proveedorProducto) {
        proveedorProducto.setEstadoId(ConstEstados.INACTIVO);
        provProductoRepository.save(proveedorProducto);
    }

    @Override
    public List<JsonProveedoresProducto> findByActive() {
        return provProductoRepository.findByActive();
    }
    
}
