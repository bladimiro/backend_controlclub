/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Usuario;
import com.smartdev.controlclub.model.JsonUsuario;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class UsuarioServices implements UsuarioInterfaces {

    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Override
    public List<Usuario> findAllActivos() {
        return usuarioRepository.findAllActivos();
    }

    @Override
    public Optional<Usuario> findById(Integer usuarioId) {
        return usuarioRepository.findById(usuarioId);
    }

    @Override
    public void insertOrUpdate(Usuario usuario) {
        usuarioRepository.save(usuario);
    }

    @Override
    public void deleteById(Integer usuarioId) {
        Usuario usuario = findById(usuarioId).get();
        usuario.setEstadoId(ConstEstados.ACTIVO);
        usuarioRepository.save(usuario);
    }

    @Override
    public Optional<Usuario> findByUsername(String username) {
        return usuarioRepository.findByUsername(username);
    }

    @Override
    public Optional<JsonUsuario> verifyByLogin(JsonUsuario user) {
        return usuarioRepository.verifyLogin(user.getUsername(), user.getPassword());
    }

    @Override
    public void cambiarEstado(Integer usuarioId, boolean habilitado) {
        Usuario usuario = usuarioRepository.findById(usuarioId).get();
        usuario.setHabilitado(habilitado);
        usuarioRepository.save(usuario);
    }
    
}
