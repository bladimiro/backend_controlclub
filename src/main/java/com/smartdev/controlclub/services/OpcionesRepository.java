/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Opciones;
import com.smartdev.controlclub.model.JsonOpciones;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface OpcionesRepository extends JpaRepository<Opciones, Integer>{
    
    @Query("select new com.smartdev.controlclub.model.JsonOpciones(o.opcionId, " +
            "o.nombre, o.enlace, o.orden) from Opciones o " +
            "where o.estadoId=1000 and o.opcionRefId = :opcionId " +
            "order by o.orden ")
    List<JsonOpciones> findByOpcionRefenciaId(@Param("opcionId") Integer opcionId);
    
    @Query(value = "select o.opcion_id, o.nombre, o.enlace, o.orden  " +
            "from opciones o " +
            "where o.estado_id=1000 and o.opcion_ref_id=:referenciaId " +
            "and opcion_id in (select op.opcion_id " +
            "from perfiles_opciones op " +
            "inner join perfiles_usuarios pu " +
            "on op.perfil_id = pu.perfil_id " +
            "where op.estado_id = 1000 " +
            "and pu.estado_id = 1000 " +
            "and pu.usuario_id = :usuarioId) " + 
            "order by o.orden", nativeQuery = true)
    List<Object[]> findByOpcionReferenciaIdUsuarioId(@Param("usuarioId") Integer usuarioId, @Param("referenciaId")  Integer referenciaId);
}
