/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Gasto;
import com.smartdev.controlclub.model.JsonGasto;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface GastosInterfaces {
    List<JsonGasto> findAllActivos();
    Optional<Gasto> findById(Integer id);
    void insertOrUpdate(Gasto gasto);
    void deleteById(Integer id);
    List<JsonGasto> findByFecha(Date fecha);
}
