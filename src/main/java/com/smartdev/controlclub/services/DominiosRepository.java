/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Dominios;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface DominiosRepository extends JpaRepository<Dominios, Integer>{
    
    @Query("select d from Dominios d where d.estadoId=1000 and d.dominio = :dominio")
    List<Dominios> findByDominio(@Param("dominio") String dominio);
}
