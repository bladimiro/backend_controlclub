/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Usuario;
import com.smartdev.controlclub.model.JsonUsuario;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{
    
    @Query("select u from Usuario u where u.estadoId=1000")
    public List<Usuario> findAllActivos();
    
    @Query("select u from Usuario u where u.estadoId=1000 and u.username=:username")
    public Optional<Usuario> findByUsername(@Param("username") String username);
    
    @Query("select new com.smartdev.controlclub.model.JsonUsuario(u.usuarioId, u.username, u.password) " +
            " from Usuario u where u.estadoId=1000 and " +
            "u.username=:username and u.password=:password")
    Optional<JsonUsuario> verifyLogin(@Param("username") String username, @Param("password") String password);
}
