/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.model.JsonOpciones;
import java.util.List;

/**
 *
 * @author bladimir
 */
public interface OpcionesInterfaces {
    List<JsonOpciones> findByOpcionRefenciaId(Integer opcionId);
    List<JsonOpciones> findByOpcionReferenciaIdUsuarioId(Integer usuarioId, Integer referenciaId);
}
