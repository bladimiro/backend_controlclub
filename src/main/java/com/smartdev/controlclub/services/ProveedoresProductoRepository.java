/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.ProveedorProducto;
import com.smartdev.controlclub.model.JsonProveedoresProducto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface ProveedoresProductoRepository extends JpaRepository<ProveedorProducto, Integer>{
    
    @Query("select new com.smartdev.controlclub.model.JsonProveedoresProducto(p.provProductoId, " +
            "p.cantidad, p.fechaCompra, p.precio, p.productoId.productoId, p.productoId.nombre, " +
            "p.proveedorId.proveedorId, p.proveedorId.nombre) from ProveedorProducto p " +
            "where p.estadoId=1000 and p.productoId.productoId = :productoId")
    List<JsonProveedoresProducto> findByProductoId(@Param("productoId") Integer productoId);
    
    @Query("select new com.smartdev.controlclub.model.JsonProveedoresProducto(p.provProductoId, " +
            "p.cantidad, p.fechaCompra, p.precio, p.productoId.productoId, p.productoId.nombre, " +
            "p.proveedorId.proveedorId, p.proveedorId.nombre) from ProveedorProducto p where p.estadoId=1000 " +
            "order by p.fechaCompra desc")
    List<JsonProveedoresProducto> findByActive();
}