/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleInventario;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface DetalleInventarioRepository extends JpaRepository<DetalleInventario, Integer>{
    
    @Query("select d from DetalleInventario d where d.estadoId=1000 and " +
            "d.inventarioId.inventarioId = :inventarioId")
    List<DetalleInventario> findByInventarioId(@Param("inventarioId") Integer inventarioId);
    
    @Query("select d from DetalleInventario d where d.estadoId=1000 and " +
            "d.inventarioId.inventarioId = :inventarioId and " +
            "d.productoId.productoId = :productoId")
    Optional<DetalleInventario> findByInventarioIdProductoId(Integer inventarioId, Integer productoId);
}
