/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.ComandasMeseros;
import com.smartdev.controlclub.model.JsonComandasMeseros;
import com.smartdev.controlclub.utils.ConstEstadoInventario;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class ComandasMeserosServices implements ComandasMeserosInterfaces {
    
    @Autowired
    private ComandasMeserosRepository comandaMeseroRepository;

    @Override
    public Optional<ComandasMeseros> findAbiertoByUsuarioId(Integer usuarioId) {
        return comandaMeseroRepository.findAbiertoByUsuarioId(usuarioId);
    }

    @Override
    public List<ComandasMeseros> findCerradoByEstadoRegistroId(Integer usuarioId, Integer estadoRegistroId) {
        return comandaMeseroRepository.findCerradoByEstadoRegistroId(usuarioId, estadoRegistroId);
    }

    @Override
    public List<ComandasMeseros> findByEstadoRegistroIdFechas(Integer estadoRegistroId, Date fechaInicio, Date fechaFin) {
        return comandaMeseroRepository.findByEstadoRegistroIdFechas(estadoRegistroId, fechaInicio, fechaFin);
    }

    @Override
    public void insertOrUpdate(ComandasMeseros comandaMesero) {
        comandaMeseroRepository.save(comandaMesero);
    }

    @Override
    public Optional<ComandasMeseros> findById(Integer id) {
        return comandaMeseroRepository.findById(id);
    }

    @Override
    public void cerrarComandaMesero(Integer usuarioId) {
        ComandasMeseros comanda = findAbiertoByUsuarioId(usuarioId).get();
        comanda.setEstadoRegistroId(ConstEstadoInventario.CERRADO);
        comanda.setFechaCierre(new Date());
        comandaMeseroRepository.save(comanda);
    }

    @Override
    public List<JsonComandasMeseros> obtenerTotalesComandasByFecha(Date fecha) {
        List<Object[]> totalesComandas = comandaMeseroRepository.obtenerTotalesComandasByFecha(fecha);
        List<JsonComandasMeseros> resultados = new ArrayList<>();
        JsonComandasMeseros comanda;
        for(Object[] obj : totalesComandas){
            comanda = new JsonComandasMeseros();
            comanda.setUsuarioId(new Integer(obj[0].toString()));
            comanda.setUsername(obj[1].toString());
            comanda.setTotal(new Double(obj[2].toString()));
            resultados.add(comanda);
        }
        return resultados;
    }

    @Override
    public Double obtenerTotal(Date fecha) {
        List<Object[]> totales = comandaMeseroRepository.obtenerTotal(fecha);
        Double total = 0D;
        System.out.println(totales.size());
        for(Object[] obj : totales) {
            System.out.println(obj.toString());
            total = new Double(obj[0].toString());
        }
        return total;
    }
    
}
