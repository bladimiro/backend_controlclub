/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Perfiles;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface PerfilesInterfaces {
    List<Perfiles> findByActives();
    Optional<Perfiles> findById(Integer perfilId);
}
