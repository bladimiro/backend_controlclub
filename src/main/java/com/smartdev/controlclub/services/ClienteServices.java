/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Cliente;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class ClienteServices implements ClienteInterfaces {
    
    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public List<Cliente> findByActives() {
        return clienteRepository.findByActives();
    }

    @Override
    public Optional<Cliente> findById(Integer id) {
        return clienteRepository.findById(id);
    }

    @Override
    public void insertOrUpdate(Cliente cliente) {
        clienteRepository.save(cliente);
    }

    @Override
    public void delete(Cliente cliente) {
        cliente.setEstadoId(ConstEstados.INACTIVO);
        clienteRepository.save(cliente);
    }

    @Override
    public Optional<Cliente> findByDocumentoIdentidad(String nit) {
        return clienteRepository.findByDocumentoIdentidad(nit);
    }
    
}