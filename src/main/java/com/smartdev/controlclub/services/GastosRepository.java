/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Gasto;
import com.smartdev.controlclub.model.JsonGasto;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface GastosRepository extends JpaRepository<Gasto, Integer> {
    
    @Query("select new com.smartdev.controlclub.model.JsonGasto(g.gastoId, " +
            "g.tipoGastoId.tipoGastoId, g.tipoGastoId.descripcion, " +
            "g.descripcion, g.fechaGasto, g.monto) from Gasto g where g.estadoId=1000")
    public List<JsonGasto> findAllActivos();
    
    @Query("select new com.smartdev.controlclub.model.JsonGasto(g.gastoId, " +
            "g.tipoGastoId.tipoGastoId, g.tipoGastoId.descripcion, " +
            "g.descripcion, g.fechaGasto, g.monto) from Gasto g " +
            "where g.estadoId=1000 and date(g.fechaGasto) = :fecha")
    List<JsonGasto> findByFecha(@Param("fecha") Date fecha);
}
