/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.ComandasMeseros;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface ComandasMeserosRepository extends JpaRepository<ComandasMeseros, Integer>{
    
    @Query("select c from ComandasMeseros c where c.estadoId=1000 and " +
            "c.usuarioId.usuarioId = :usuarioId and c.estadoRegistroId=1006")
    Optional<ComandasMeseros> findAbiertoByUsuarioId(@Param("usuarioId") Integer usuarioId);
    
    @Query("select c from ComandasMeseros c where c.estadoId=1000 and " +
            "c.usuarioId.usuarioId = :usuarioId and c.estadoRegistroId = :estadoRegistroId")
    List<ComandasMeseros> findCerradoByEstadoRegistroId(@Param("usuarioId") Integer usuarioId, @Param("estadoRegistroId") Integer estadoRegistroId);
    
    @Query("select c from ComandasMeseros c where c.estadoId=1000 and " +
            "c.estadoRegistroId = :estadoRegistroId and " +
            "(date(c.fechaRegistro) between :fechaInicio and :fechaFin)")
    List<ComandasMeseros> findByEstadoRegistroIdFechas(@Param("estadoRegistroId") Integer estadoRegistroId, 
            @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
    
    @Query(value = "select us.usuario_id,us.username, sum(monto_total) " +
            "from comandas_meseros cm " +
            "inner join detalle_comandas dc " +
            "on cm.comanda_mesero_id = dc.comanda_mesero_id " +
            "inner join pedido pe " +
            "on dc.pedido_id = pe.pedido_id " +
            "inner join usuario us " +
            "on cm.usuario_id = us.usuario_id " +
            "where cm.estado_id=1000 " +
            "and dc.estado_id=1000 " +
            "and cm.estado_registro_id=1007 " +
            "and date(cm.fecha_registro)=:fecha " +
            "and pe.estado_id=1000 " +
            "group by us.usuario_id,us.username", nativeQuery = true)
    List<Object[]> obtenerTotalesComandasByFecha(@Param("fecha") Date fecha);
    
    @Query(value = "select sum(monto_total) total " +
        "from comandas_meseros cm " +
        "inner join detalle_comandas dc " +
        "on cm.comanda_mesero_id = dc.comanda_mesero_id " +
        "inner join pedido pe " +
        "on dc.pedido_id = pe.pedido_id " +
        "where cm.estado_id=1000 " +
        "and dc.estado_id=1000 " +
        "and cm.estado_registro_id=1007 " +
        "and pe.tipo_pago_id=1011 " +
        "and date(cm.fecha_registro)= :fecha " +
        "and pe.estado_id=1000", nativeQuery = true)
    List<Object[]> obtenerTotal(@Param("fecha") Date fecha);
}
