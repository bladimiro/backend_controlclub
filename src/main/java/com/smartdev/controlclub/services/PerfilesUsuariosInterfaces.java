/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.PerfilesUsuarios;
import com.smartdev.controlclub.model.JsonOpciones;
import com.smartdev.controlclub.model.JsonPerfilesUsuarios;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface PerfilesUsuariosInterfaces {
    List<JsonOpciones> findOpcionesPrincipalesByUsuarioId(Integer usuarioId);
    List<JsonPerfilesUsuarios> findPerfilesByUsuarioId(Integer usuarioId);
    Optional<PerfilesUsuarios> findById(Integer perfilId);
    void insertOrUpdate(PerfilesUsuarios perfilUsuario);
    void delete(PerfilesUsuarios perfilUsuario);
}
