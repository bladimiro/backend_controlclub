/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Usuario;
import com.smartdev.controlclub.model.JsonUsuario;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface UsuarioInterfaces {
    List<Usuario> findAllActivos();
    Optional<Usuario> findById(Integer usuarioId);
    void insertOrUpdate(Usuario usuario);
    void deleteById(Integer usuarioId);
    Optional<Usuario> findByUsername(String username);
    Optional<JsonUsuario> verifyByLogin(JsonUsuario user);
    void cambiarEstado(Integer usuarioId, boolean habilitado);
}
