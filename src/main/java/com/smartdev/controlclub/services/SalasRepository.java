/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Sala;
import com.smartdev.controlclub.model.JsonSala;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface SalasRepository extends JpaRepository<Sala, Integer>{
    
    @Query("select new com.smartdev.controlclub.model.JsonSala(s.salaId, s.empresaId.empresaId, s.nombreSala, s.turno) " +
            " from Sala s where s.estadoId = 1000 and s.empresaId.empresaId = :empresaId")
    List<JsonSala> findByEmpresaId(@Param("empresaId") Integer empresaId);
}
