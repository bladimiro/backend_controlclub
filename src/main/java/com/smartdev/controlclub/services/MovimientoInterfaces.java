/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Movimiento;
import com.smartdev.controlclub.model.JsonMovimiento;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface MovimientoInterfaces {
    List<JsonMovimiento> findByActives();
    Optional<Movimiento> findById(Integer movimientoId);
    Optional<Movimiento> findByGastoId(Integer gastoId);
    Optional<Movimiento> findByPedidoId(Integer pedidoId);
    List<JsonMovimiento> findByFechas(Date fechaInicio, Date fechaFin);
    void insertOrUpdate(Movimiento movimiento);
    void delete(Movimiento movimiento);
}
