/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetallePedido;
import com.smartdev.controlclub.model.JsonDetallePedido;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface DetallePedidoRepository extends JpaRepository<DetallePedido, Integer>{
    //, Integer cantidad, double precioUnitario, String nombreSubproducto
    /*@Query("select new com.smartdev.controlclub.model.JsonDetallePedido(d.detallePedidoId, "+
            "d.cantidad, d.precioUnitario, d.subproductoId.nombre) from DetallePedido d "+
            "where d.estadoId=1000 and d.pedidoId.pedidoId = :pedidoId")*/
    @Query("select d from DetallePedido d where d.estadoId=1000 and d.pedidoId.pedidoId = :pedidoId")
    List<DetallePedido> findByPedidoId(Integer pedidoId);
}
