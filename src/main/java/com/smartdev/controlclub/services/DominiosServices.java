/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Dominios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bladimir
 */
@Service
public class DominiosServices implements DominiosInterfaces {

    @Autowired
    private DominiosRepository dominioRepository;
    
    @Override
    public List<Dominios> findByDominio(String dominio) {
        return dominioRepository.findByDominio(dominio);
    }
    
}
