/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Sala;
import com.smartdev.controlclub.model.JsonSala;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class SalasServices implements SalasInterfaces {

    @Autowired
    private SalasRepository salaRepository;
            
    @Override
    public List<JsonSala> findByEmpresaId(Integer empresaId) {
        return salaRepository.findByEmpresaId(empresaId);
    }

    @Override
    public Optional<Sala> findById(Integer salaId) {
        return salaRepository.findById(salaId);
    }

    @Override
    public void insertOrUpdate(Sala sala) {
        salaRepository.save(sala);
    }

    @Override
    public void delete(Sala sala) {
        sala.setEstadoId(ConstEstados.INACTIVO);
        salaRepository.save(sala);
    }
    
}
