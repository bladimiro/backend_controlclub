/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Empresa;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface EmpresaInterfaces {
    Optional<Empresa> findById(Integer id);
    void insertOrUpdate(Empresa empresa);
    Empresa findFirst();
}
