/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Inventario;
import com.smartdev.controlclub.model.JsonInventario;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author bladimir
 */
@Repository
public interface InventariosRepository extends JpaRepository<Inventario, Integer>{
    
    @Query("select i from Inventario i where i.estadoId=1000 and i.estadoInventarioId = :estadoInventarioId")
    Optional<Inventario> findByEstadoInventario(@PathVariable("estadoInventarioId") Integer estadoInventarioId);
    
    @Query("select new com.smartdev.controlclub.model.JsonInventario(i.inventarioId, " +
            "i.estadoInventarioId, i.fechaInicio, i.fechaCierre, i.usuarioId.usuarioId, " +
            "i.usuarioCierreId.usuarioId) from Inventario i where i.estadoId=1000 " +
            " and i.estadoInventarioId=1007")
    List<JsonInventario> findByEstadoHistorico();
}
