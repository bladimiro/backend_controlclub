/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Inventario;
import com.smartdev.controlclub.model.JsonInventario;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface InventariosInterfaces {
    Optional<Inventario> findByEstadoInventario(Integer estadoInventarioId);
    Optional<Inventario> findById(Integer inventarioId);
    void insertOrUpdate(Inventario inventario);
    List<JsonInventario> findByEstadoHistorico();
    JsonInventario findDetalleById(Integer inventarioId);
    void cerrarInventario(Inventario inventario, Integer usuarioId);
}
