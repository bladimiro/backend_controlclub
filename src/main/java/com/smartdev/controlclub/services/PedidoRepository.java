/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Pedido;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Integer>{
    
    @Query("select p from Pedido p where p.estadoId=1000")
    List<Pedido> findAllActives();
    
    @Query("select p from Pedido p where p.estadoId=1000 and p.estadoPedidoId = :estadoPedidoId" )
    List<Pedido> findByEstadoPedidoId(@Param("estadoPedidoId") Integer estadoPedidoId);
    
    @Query("select p from Pedido p where p.estadoId=1000 and p.estadoPedidoId = :estadoPedidoId " +
           "and (date(p.fechaRegistro) between :fechaInicio and :fechaFin) ")
    List<Pedido> findByEstadoPedidoIdFechas(@Param("estadoPedidoId") Integer estadoPedidoId, 
            @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
    
    @Query("select max(p.numeroPedido) from Pedido p")
    Integer obtenerMaximo();
    
    @Query("select p from Pedido p where p.estadoId=1000 and p.tipoPagoId = :tipoPagoId")
    List<Pedido> findByTipoPagoId(@Param("tipoPagoId") Integer tipoPagoId);
    
    @Query("select p from Pedido p where p.estadoId=1000 and p.estadoPedidoId=1015 and " +
            "p.usuarioId.usuarioId = :usuarioId ")
    Optional<Pedido> findByUsuarioIdEstadoPedidoId(Integer usuarioId);
}