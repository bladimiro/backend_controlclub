/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Pedido;
import com.smartdev.controlclub.model.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface PedidoInterface {
    List<Pedido> findAllActives();
    Optional<Pedido> findById(Integer id);
    void insertOrUpdate(Pedido pedido);
    void delete(Pedido pedido);
    List<JsonPedidosRespuesta> findByEstadoPedidoId(Integer estadoPedidoId);
    List<JsonPedidosRespuesta> findByEstadoPedidoIdFechas(Integer estadoPedidoId, Date fechaInicio, Date fechaFin);
    JsonRespuestaPedido enviarPedido(JsonPedidos pedido);
    Optional<Pedido> findByUsuarioIdEstadoPedidoId(Integer usuarioId);
    JsonRespuestaPedido enviarPedidoVasoChica(JsonPedidos pedido);
    void cancelarPedido(JsonPedidos pedido);
    void atenderPedido(JsonPedidos pedido);
    List<JsonPedidosRespuesta> findByTipoPagoId(Integer tipoPagoId);
    void guardarPedido(JsonPedidos jpedido);
    void agregarPedido(JsonPedidos jpedido);
    void guardarPedidoVasoChica(JsonPedidos jpedido);
    void agregarPedidoVasoChica(JsonPedidos jpedido);
    void consolidarPedido(JsonPedidos jpedido);
    void pagarPedido(JsonPedidos jpedido);
    void imprimirPedido(JsonPedidos jpedido);
}
