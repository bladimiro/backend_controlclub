/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleComandas;
import com.smartdev.controlclub.model.JsonDetalleComanda;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface DetalleComandasInterfaces {
    List<JsonDetalleComanda> findByComandaMeseroId(Integer comandaMeseroId);
    void insertOrUpdate(DetalleComandas detalleComanda);
    Optional<DetalleComandas> findById(Integer id);
    List<JsonDetalleComanda> findByUsuarioIdFecha(Integer usuarioId, Date fecha);
    //void cambiarEstado(DetalleComandas detalleComanda);
}
