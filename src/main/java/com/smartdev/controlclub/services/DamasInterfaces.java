/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Damas;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface DamasInterfaces {
    List<Damas> findByActives();
    List<Damas> findByHabilitado(boolean habilitada);
    Optional<Damas> findById(Integer id);
    void cambiarEstado(Integer damaId, boolean estado);
    void insertOrUpdate(Damas dama);
    Optional<Damas> findByCodigo(String codigo);
}
