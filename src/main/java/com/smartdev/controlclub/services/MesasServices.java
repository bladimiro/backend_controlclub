/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Mesa;
import com.smartdev.controlclub.model.JsonMesa;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 * 
 */
@Service
@Transactional
public class MesasServices implements MesasInterfaces {
    
    @Autowired 
    private MesasRepository mesaRepository;

    @Override
    public List<JsonMesa> findBySalaId(Integer salaId) {
        return mesaRepository.findBySalaId(salaId);
    }

    @Override
    public Optional<Mesa> findById(Integer mesaId) {
        return mesaRepository.findById(mesaId);
    }

    @Override
    public void insertOrUpdate(Mesa mesa) {
        mesaRepository.save(mesa);
    }

    @Override
    public void delete(Mesa mesa) {
        mesa.setEstadoId(ConstEstados.INACTIVO);
        mesaRepository.save(mesa);
    }

    @Override
    public List<JsonMesa> findAllActives() {
        return mesaRepository.findAllActives();
    }
    
    
}
