/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Subproducto;
import com.smartdev.controlclub.model.JsonSubproducto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface SubproductoRepository extends JpaRepository<Subproducto, Integer>{
    
    @Query("select new com.smartdev.controlclub.model.JsonSubproducto(s.subproductoId, "+
            "s.productoId.productoId, s.nombre, s.tipoId, s.precio, s.equivalencia, s.cantidadChicas, s.tipoSolicitudId) " +
            "from Subproducto s where s.estadoId = 1000 and s.productoId.estadoId=1000 ")
    List<JsonSubproducto> findAllActives();
    
    @Query("select new com.smartdev.controlclub.model.JsonSubproducto(s.subproductoId, " +
            "s.productoId.productoId, s.nombre, s.tipoId, s.precio, s.equivalencia, " +
            " s.cantidadChicas, s.tipoSolicitudId, s.comision) from Subproducto s " +
            "where s.estadoId = 1000 and s.productoId.productoId = :productoId ")
    List<JsonSubproducto> findByProductoId(@Param("productoId") Integer productoId);
    
    @Query("select new com.smartdev.controlclub.model.JsonSubproducto(s.subproductoId, " +
            "s.productoId.productoId, s.nombre, s.tipoId, s.precio, s.equivalencia, " +
            " s.cantidadChicas, s.tipoSolicitudId) from Subproducto s " +
            "where s.estadoId = 1000 and s.tipoId = :tipoId and s.tipoSolicitudId = :tipoSolicitudId ")
    List<JsonSubproducto> findByTipoIdTiSolicitante(@Param("tipoId") Integer tipoId, @Param("tipoSolicitudId") Integer tipoSolicitudId);
}
