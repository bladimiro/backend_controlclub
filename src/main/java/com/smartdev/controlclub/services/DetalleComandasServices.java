/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleComandas;
import com.smartdev.controlclub.model.JsonDetalleComanda;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class DetalleComandasServices implements DetalleComandasInterfaces {

    @Autowired
    private DetalleComandasRepository detalleComandaRepository;
    
    @Autowired
    private DetallePedidoServices detallePedidoService;
    
    @Override
    public List<JsonDetalleComanda> findByComandaMeseroId(Integer comandaMeseroId) {
        List<DetalleComandas> detalleList = detalleComandaRepository.findByComandaMeseroId(comandaMeseroId);
        List<JsonDetalleComanda> respuesta = new ArrayList<>();
        JsonDetalleComanda detalle;
        for(DetalleComandas deta : detalleList){
            detalle = new JsonDetalleComanda();
            detalle.setDetalleComandaId(deta.getDetalleComandaId());
            detalle.setComandaMeseroId(deta.getDetalleComandaId());
            detalle.setPedidoId(deta.getPedidoId().getPedidoId());
            detalle.setNroPedido(deta.getPedidoId().getNumeroPedido());
            detalle.setTipoPagoId(deta.getPedidoId().getTipoPagoId());
            detalle.setEstadoPedidoId(deta.getPedidoId().getEstadoPedidoId());
            detalle.setFechaRegistro(deta.getFechaRegistro());
            detalle.setDatosPedido(detallePedidoService.findByPedidoId(deta.getPedidoId().getPedidoId()));
            respuesta.add(detalle);
        }
        return respuesta;
    }

    @Override
    public void insertOrUpdate(DetalleComandas detalleComanda) {
        detalleComandaRepository.save(detalleComanda);
    }

    @Override
    public Optional<DetalleComandas> findById(Integer id) {
        return detalleComandaRepository.findById(id);
    }

    @Override
    public List<JsonDetalleComanda> findByUsuarioIdFecha(Integer usuarioId, Date fecha) {
        List<DetalleComandas> detalleList = detalleComandaRepository.findByUsuarioIdFecha(usuarioId, fecha);
        List<JsonDetalleComanda> respuesta = new ArrayList<>();
        JsonDetalleComanda detalle;
        for(DetalleComandas deta : detalleList){
            detalle = new JsonDetalleComanda();
            detalle.setDetalleComandaId(deta.getDetalleComandaId());
            detalle.setComandaMeseroId(deta.getDetalleComandaId());
            detalle.setPedidoId(deta.getPedidoId().getPedidoId());
            detalle.setNroPedido(deta.getPedidoId().getNumeroPedido());
            detalle.setTipoPagoId(deta.getPedidoId().getTipoPagoId());
            detalle.setEstadoPedidoId(deta.getPedidoId().getEstadoPedidoId());
            detalle.setFechaRegistro(deta.getFechaRegistro());
            detalle.setDatosPedido(detallePedidoService.findByPedidoId(deta.getPedidoId().getPedidoId()));
            respuesta.add(detalle);
        }
        return respuesta;
    }
    
}
