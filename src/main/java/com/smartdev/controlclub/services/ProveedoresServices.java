/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Proveedor;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class ProveedoresServices implements ProveedoresInterfaces {

    @Autowired
    private ProveedoresRepository proveedorRepository;
    
    @Override
    public List<Proveedor> findAllActives() {
        return proveedorRepository.findAllActives();
    }

    @Override
    public Optional<Proveedor> findById(Integer id) {
        return proveedorRepository.findById(id);
    }

    @Override
    public void insertOrUpdate(Proveedor proveedor) {
        proveedorRepository.save(proveedor);
    }

    @Override
    public void delete(Proveedor proveedor) {
        proveedor.setEstadoId(ConstEstados.INACTIVO);
        proveedorRepository.save(proveedor);
    }
    
}
