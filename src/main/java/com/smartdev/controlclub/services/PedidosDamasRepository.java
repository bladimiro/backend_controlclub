/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.PedidosDamas;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface PedidosDamasRepository extends JpaRepository<PedidosDamas, Integer>{
    
    @Query("select p from PedidosDamas p where p.estadoId = :estadoId")
    List<PedidosDamas> findByEstadoId(Integer estadoId);
    
    @Query("select p from PedidosDamas p where p.estadoId=1000 and " +
            "p.detallePedidoId.detallePedidoId = :detallePedidoId")
    List<PedidosDamas> findByDetallePedidoId(@Param("detallePedidoId") Integer detallePedidoId);
    
    @Query("select p from PedidosDamas p where p.estadoId=1000 and " +
            "p.damaId.damaId = :damaId and (date(p.fechaRegistro) between :fechaInicio and :fechaFin)")
    List<PedidosDamas> findByDamaIdFecha(@Param("damaId") Integer damaId, @Param("fechaInicio") Date fechaInicio, 
            @Param("fechaFin") Date fechaFin);
}
