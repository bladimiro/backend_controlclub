/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Cliente;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
    
    @Query("select c from Cliente c where c.estadoId=1000")
    List<Cliente> findByActives();
    
    @Query("select c from Cliente c where c.estadoId=1000 and c.nit=:nit")
    Optional<Cliente> findByDocumentoIdentidad(String nit);
}
