/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Producto;
import com.smartdev.controlclub.model.JsonRespuestaGeneral;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface ProductosInterfaces {
    List<Producto> findAllActives();
    Optional<Producto> findById(Integer productoId);
    void insertOrUpdate(Producto producto);
    void delete(Producto producto);
    JsonRespuestaGeneral existeRelacion(Integer productoId);
}
