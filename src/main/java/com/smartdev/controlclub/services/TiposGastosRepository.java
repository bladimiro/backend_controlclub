/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.TipoGasto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface TiposGastosRepository extends JpaRepository<TipoGasto, Integer>{
    @Query("select t from TipoGasto t where t.estadoId=1000")
    List<TipoGasto> findAllActivos();
}
