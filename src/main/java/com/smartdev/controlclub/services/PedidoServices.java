/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.ComandasMeseros;
import com.smartdev.controlclub.entities.DetalleComandas;
import com.smartdev.controlclub.entities.DetalleInventario;
import com.smartdev.controlclub.entities.DetallePedido;
import com.smartdev.controlclub.entities.Inventario;
import com.smartdev.controlclub.entities.Movimiento;
import com.smartdev.controlclub.entities.Pedido;
import com.smartdev.controlclub.entities.PedidosDamas;
//import com.smartdev.controlclub.entities.Subproducto;
//import com.smartdev.controlclub.entities.Subproducto;
import com.smartdev.controlclub.model.JsonDetallePedido;
import com.smartdev.controlclub.model.JsonPedidoDama;
import com.smartdev.controlclub.model.JsonPedidos;
import com.smartdev.controlclub.model.JsonPedidosRespuesta;
import com.smartdev.controlclub.model.JsonRespuestaPedido;
import com.smartdev.controlclub.model.JsonSubproducto;
import com.smartdev.controlclub.utils.ConstEstadoInventario;
import com.smartdev.controlclub.utils.ConstEstadoPedido;
import com.smartdev.controlclub.utils.ConstEstados;
import com.smartdev.controlclub.utils.ConstTipoPagoID;
import com.smartdev.controlclub.utils.ConstTipoSubproducto;
import com.smartdev.controlclub.utils.ConstValores;
import com.smartdev.controlclub.utils.OperacionesFecha;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class PedidoServices implements PedidoInterface {
    
    @Autowired
    private PedidoRepository pedidoRepository;
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @Autowired
    private ClienteServices clienteService;
    
    @Autowired
    private SubproductoServices subproductoService;
    
    @Autowired
    private DetallePedidoServices detalleService;
    
    @Autowired
    private MovimientoServices movimientoService;
    
    @Autowired
    private InventariosServices inventarioService;
    
    @Autowired
    private DetalleInventarioServices detalleInventarioService;
    
    @Autowired
    private DamasServices damaService;
    
    @Autowired
    private PedidosDamasServices pedidoDamaService;
    
    @Autowired
    private ComandasMeserosServices comandaMeseroService;
    
    @Autowired
    private DetalleComandasServices detalleComandoService;

    @Override
    public List<Pedido> findAllActives() {
        return pedidoRepository.findAllActives();
    }

    @Override
    public Optional<Pedido> findById(Integer id) {
        return pedidoRepository.findById(id);
    }

    @Override
    public void insertOrUpdate(Pedido pedido) {
        pedidoRepository.save(pedido);
    }

    @Override
    public void delete(Pedido pedido) {
        pedido.setEstadoId(ConstEstados.INACTIVO);
    }

    @Override
    public List<JsonPedidosRespuesta> findByEstadoPedidoId(Integer estadoPedidoId) {
        List<Pedido> pedidosList = pedidoRepository.findByEstadoPedidoId(estadoPedidoId);
        List<JsonPedidosRespuesta> respuesta = new ArrayList<>();
        JsonPedidosRespuesta jpedidos;
        
        for(Pedido ped : pedidosList){
            jpedidos = new JsonPedidosRespuesta();
            jpedidos.setPedidoId(ped.getPedidoId());
            if(ped.getClienteId() != null)
                jpedidos.setClienteId(ped.getClienteId().getClienteId());
            if(ped.getMesaId() != null){
                jpedidos.setMesaId(ped.getMesaId().getMesaId());
                jpedidos.setNumeroMesa(ped.getMesaId().getNumero());
            }
            if(ped.getUsuarioId() != null){
                jpedidos.setUsuarioId(ped.getUsuarioId().getUsuarioId());
                jpedidos.setUsername(ped.getUsuarioId().getUsername());
            }
            jpedidos.setNumeroPedido(ped.getNumeroPedido());
            jpedidos.setMontoTotal(ped.getMontoTotal());
            jpedidos.setMontoEntregado(ped.getMontoEntregado());
            jpedidos.setTipoPagoId(ped.getTipoPagoId());
            jpedidos.setFechaRegistro(ped.getFechaRegistro());
            jpedidos.setDetalle(ped.getDetallePedidoList().stream().map(p -> new JsonDetallePedido(p.getDetallePedidoId(), 
                    p.getCantidad(), p.getPrecioUnitario(), p.getPedidoId().getPedidoId(), 
                    p.getSubproductoId().getSubproductoId(), p.getSubproductoId().getNombre(), 
                    p.getUsuarioId().getUsuarioId(), this.obtenerDamas(p.getDetallePedidoId())))
                    .collect(Collectors.toList()));
            jpedidos.setObservacion(ped.getObservacion());
            respuesta.add(jpedidos);
        }
        return respuesta;
    }
    
    private String obtenerDamasComisiones(Integer detallePedidoId, Integer cantidad, Double comiProduc){
        List<JsonPedidoDama> pedidosDama = pedidoDamaService.findByDetallePedidoId(detallePedidoId).stream().map(d -> 
                    new JsonPedidoDama(d.getPedidoDamaId(), d.getPedidoDamaId(), d.getDetallePedidoId().getDetallePedidoId(),
                       d.getDamaId().getNombre())).collect(Collectors.toList());
        if(pedidosDama.isEmpty())
            return "";
        else {
            /*System.out.println(comiProduc);
            System.out.println("comision");
            System.out.println(cantidad);*/
            Double comision = comiProduc * cantidad;
            //System.out.println(comision);
            String concatenado = " (";
                String separador = "";
                for(JsonPedidoDama jped : pedidosDama) {//<Link to='#' onClick={() => this.abrirModalChica(1)}>Mostrar</Link>
                    concatenado += separador + 
                            jped.getNombreDama() + " - " + comision;
                    separador =  ", ";
                }
                concatenado += ")";
            return concatenado;
        }
    }
    
    private String obtenerDamas(Integer detallePedidoId){
        List<JsonPedidoDama> pedidosDama = pedidoDamaService.findByDetallePedidoId(detallePedidoId).stream().map(d -> 
                    new JsonPedidoDama(d.getPedidoDamaId(), d.getPedidoDamaId(), d.getDetallePedidoId().getDetallePedidoId(),
                       d.getDamaId().getNombre())).collect(Collectors.toList());
        if(pedidosDama.isEmpty())
            return "";
        else {
            String concatenado = " (";
                String separador = "";
                for(JsonPedidoDama jped : pedidosDama) {
                    concatenado += separador + jped.getNombreDama();
                    separador = ", ";
                }
                concatenado += ")";
            return concatenado;
        }
        
    }

    @Override
    public JsonRespuestaPedido enviarPedidoVasoChica(JsonPedidos objPedidos){
        JsonRespuestaPedido respuesta = new JsonRespuestaPedido();
        DetallePedido detalle;
        PedidosDamas pedidoDama;
        Pedido pedido = new Pedido();
        
        // registro pedido
        pedido.setFechaRegistro(new Date());
        pedido.setEstadoId(ConstEstados.ACTIVO);
        pedido.setClienteId(clienteService.findById(0).get());
        //pedido.setMesaId(mesaService.findById(objPedidos.getMesaId()).get());
        pedido.setUsuarioId(usuarioService.findById(objPedidos.getUsuarioId()).get());
        pedido.setEstadoPedidoId(ConstEstadoPedido.SOLICITADO);
        pedido.setNumeroPedido((pedidoRepository.obtenerMaximo()+1));
        pedido.setMontoTotal(objPedidos.getMontoTotal());
        pedido.setMontoEntregado(objPedidos.getMontoEntregado());
        pedido.setTipoPagoId(objPedidos.getTipoPagoId());
        
        pedidoRepository.save(pedido);
        
        // registro detalle
        for(JsonSubproducto subpro : objPedidos.getSubproductos()){
            detalle = new DetallePedido();
            detalle.setPedidoId(pedido);
            detalle.setUsuarioId(pedido.getUsuarioId());
            detalle.setSubproductoId(subproductoService.findById(subpro.getSubproductoId()).get());
            detalle.setCantidad(subpro.getCantidad());
            detalle.setPrecioUnitario(subpro.getPrecio());
            detalle.setFechaRegistro(new Date());
            detalle.setEstadoId(ConstEstados.ACTIVO);
            
            detalleService.insertOrUpdate(detalle);
            
            pedidoDama = new PedidosDamas();
            pedidoDama.setDamaId(damaService.findById(subpro.getDamaId()).get());
            pedidoDama.setDetallePedidoId(detalle);
            pedidoDama.setFechaRegistro(new Date());
            pedidoDama.setEstadoId(ConstEstados.ACTIVO);
            pedidoDamaService.insertOrUpdate(pedidoDama);
        }
        
        // 
        Optional<ComandasMeseros> comandaVig = comandaMeseroService.findAbiertoByUsuarioId(objPedidos.getUsuarioId());
        ComandasMeseros comanda;
        if(comandaVig.isPresent()){
            comanda = comandaVig.get();
        } else {
            comanda = new ComandasMeseros();
            comanda.setUsuarioId(pedido.getUsuarioId());
            comanda.setEstadoRegistroId(ConstEstadoInventario.ABIERTO);
            comanda.setFechaRegistro(new Date());
            comanda.setEstadoId(ConstEstados.ACTIVO);
            comandaMeseroService.insertOrUpdate(comanda);
        }
        DetalleComandas detalleCom = new DetalleComandas();
        detalleCom.setComandaMeseroId(comanda);
        detalleCom.setPedidoId(pedido);
        detalleCom.setEstadoRegistroId(ConstEstadoPedido.SOLICITADO);
        detalleCom.setFechaRegistro(new Date());
        detalleCom.setEstadoId(ConstEstados.ACTIVO);
        detalleComandoService.insertOrUpdate(detalleCom);
        
        respuesta.setNumeroPedido(pedido.getNumeroPedido());
        respuesta.setMensaje("Solicitud realizada satisfactoriamente.");
        
        return respuesta;
    }
    
    @Override
    public JsonRespuestaPedido enviarPedido(JsonPedidos objPedidos) {
        JsonRespuestaPedido respuesta = new JsonRespuestaPedido();
        DetallePedido detalle;
        PedidosDamas pedidoDama;
        Pedido pedido = new Pedido();
        
        // registro pedido
        pedido.setFechaRegistro(new Date());
        pedido.setEstadoId(ConstEstados.ACTIVO);
        pedido.setClienteId(clienteService.findById(0).get());
        //pedido.setMesaId(mesaService.findById(objPedidos.getMesaId()).get());
        pedido.setUsuarioId(usuarioService.findById(objPedidos.getUsuarioId()).get());
        pedido.setEstadoPedidoId(ConstEstadoPedido.SOLICITADO);
        pedido.setNumeroPedido((pedidoRepository.obtenerMaximo()+1));
        pedido.setMontoTotal(objPedidos.getMontoTotal());
        pedido.setMontoEntregado(objPedidos.getMontoEntregado());
        pedido.setTipoPagoId(objPedidos.getTipoPagoId());
        
        pedidoRepository.save(pedido);
        
        // registro detalle
        for(JsonSubproducto subpro : objPedidos.getSubproductos()){
            detalle = new DetallePedido();
            detalle.setPedidoId(pedido);
            detalle.setUsuarioId(pedido.getUsuarioId());
            detalle.setSubproductoId(subproductoService.findById(subpro.getSubproductoId()).get());
            detalle.setCantidad(subpro.getCantidad());
            detalle.setPrecioUnitario(subpro.getPrecio());
            detalle.setFechaRegistro(new Date());
            detalle.setEstadoId(ConstEstados.ACTIVO);
            
            detalleService.insertOrUpdate(detalle);
            
            for(JsonPedidoDama pedDama : subpro.getPedidoDamas()){
                pedidoDama = new PedidosDamas();
                pedidoDama.setDamaId(damaService.findById(pedDama.getDamaId()).get());
                pedidoDama.setDetallePedidoId(detalle);
                pedidoDama.setFechaRegistro(new Date());
                pedidoDama.setEstadoId(ConstEstados.ACTIVO);
                pedidoDamaService.insertOrUpdate(pedidoDama);
            }
        }
        
        // 
        Optional<ComandasMeseros> comandaVig = comandaMeseroService.findAbiertoByUsuarioId(objPedidos.getUsuarioId());
        ComandasMeseros comanda;
        if(comandaVig.isPresent()){
            comanda = comandaVig.get();
        } else {
            comanda = new ComandasMeseros();
            comanda.setUsuarioId(pedido.getUsuarioId());
            comanda.setEstadoRegistroId(ConstEstadoInventario.ABIERTO);
            comanda.setFechaRegistro(new Date());
            comanda.setEstadoId(ConstEstados.ACTIVO);
            comandaMeseroService.insertOrUpdate(comanda);
        }
        DetalleComandas detalleCom = new DetalleComandas();
        detalleCom.setComandaMeseroId(comanda);
        detalleCom.setPedidoId(pedido);
        detalleCom.setEstadoRegistroId(ConstEstadoPedido.SOLICITADO);
        detalleCom.setFechaRegistro(new Date());
        detalleCom.setEstadoId(ConstEstados.ACTIVO);
        detalleComandoService.insertOrUpdate(detalleCom);
        
        respuesta.setNumeroPedido(pedido.getNumeroPedido());
        respuesta.setMensaje("Solicitud realizada satisfactoriamente.");
        
        return respuesta;
    }

    @Override
    public void cancelarPedido(JsonPedidos jpedido) {
        Pedido pedido = findById(jpedido.getPedidoId()).get();
        pedido.setEstadoPedidoId(ConstEstadoPedido.CANCELADO);
        pedido.setUsuarioCancelaId(usuarioService.findById(jpedido.getUsuarioCancelaId()).get());
        pedido.setObservacion(jpedido.getObservacion());
        pedidoRepository.save(pedido);
    }

    @Override
    public void atenderPedido(JsonPedidos jpedido) {
        // registrar datos de pedido
        Pedido pedido = findById(jpedido.getPedidoId()).get();
        pedido.setEstadoPedidoId(ConstEstadoPedido.ATENDIDO);
        pedido.setMontoEntregado(jpedido.getMontoEntregado());
        pedido.setFechaPagoCuenta(new Date());
        pedido.setTipoPagoId(jpedido.getTipoPagoId());
        pedido.setUsuarioDespachaId(usuarioService.findById(jpedido.getUsuarioDespachaId()).get());
        pedidoRepository.save(pedido);
        
        if(pedido.getTipoPagoId().equals(ConstTipoPagoID.AL_CONTADO)){
            // actualizando datos de inventarios
            Inventario inventario = inventarioService.findByEstadoInventario(ConstEstadoInventario.ABIERTO).get();
            DetalleInventario detalleInventario;
            Double cantidad_usada = 0D;
            String glosa = "";
            String separador = "";
            for(DetallePedido deta : pedido.getDetallePedidoList()){
                glosa += separador + deta.getCantidad().toString() + " " + deta.getSubproductoId().getNombre();
                separador = ", ";
                if(deta.getSubproductoId().getTipoId().equals(ConstTipoSubproducto.BOTELLA)){
                    detalleInventario = detalleInventarioService.findByInventarioIdProductoId(
                                inventario.getInventarioId(), deta.getSubproductoId().getProductoId().getProductoId()).get();
                    cantidad_usada = deta.getCantidad() * deta.getSubproductoId().getEquivalencia() +
                                       detalleInventario.getCantidadUsada();
                    detalleInventario.setCantidadUsada(cantidad_usada);
                    detalleInventarioService.insertOrUpdate(detalleInventario);
                }
                
            }
            // registrar datos de movimientos
            Movimiento movimiento = new Movimiento();
            movimiento.setTipoId(ConstValores.INGRESO);
            movimiento.setMonto(pedido.getMontoTotal());
            movimiento.setFechaMovimiento(pedido.getFechaPagoCuenta());
            movimiento.setPedidoId(pedido);
            movimiento.setGlosa(glosa);
            movimiento.setFechaRegistro(new Date());
            movimiento.setEstadoId(ConstEstados.ACTIVO);
            movimientoService.insertOrUpdate(movimiento);

        }
        
    }

    @Override
    public List<JsonPedidosRespuesta> findByEstadoPedidoIdFechas(Integer estadoPedidoId, Date fechaInicio, Date fechaFin) {
        List<Pedido> pedidosList = pedidoRepository.findByEstadoPedidoIdFechas(estadoPedidoId, fechaInicio, fechaFin);
        
        List<JsonPedidosRespuesta> respuesta = new ArrayList<>();
        JsonPedidosRespuesta jpedidos;
        
        for(Pedido ped : pedidosList){
            jpedidos = new JsonPedidosRespuesta();
            jpedidos.setPedidoId(ped.getPedidoId());
            if(ped.getClienteId() != null)
                jpedidos.setClienteId(ped.getClienteId().getClienteId());
            if(ped.getMesaId() != null){
                jpedidos.setMesaId(ped.getMesaId().getMesaId());
                jpedidos.setNumeroMesa(ped.getMesaId().getNumero());
            }
            if(ped.getUsuarioId() != null){
                jpedidos.setUsuarioId(ped.getUsuarioId().getUsuarioId());
                jpedidos.setUsername(ped.getUsuarioId().getUsername());
            }
            jpedidos.setNumeroPedido(ped.getNumeroPedido());
            jpedidos.setMontoTotal(ped.getMontoTotal());
            jpedidos.setMontoEntregado(ped.getMontoEntregado());
            jpedidos.setTipoPagoId(ped.getTipoPagoId());
            jpedidos.setFechaRegistro(ped.getFechaRegistro());
            jpedidos.setDetalle(ped.getDetallePedidoList().stream().map(p -> new JsonDetallePedido(p.getDetallePedidoId(), 
                    p.getCantidad(), p.getPrecioUnitario(), p.getPedidoId().getPedidoId(), 
                    p.getSubproductoId().getSubproductoId(), p.getSubproductoId().getNombre(), 
                    p.getUsuarioId().getUsuarioId(), this.obtenerDamasComisiones(p.getDetallePedidoId(), p.getCantidad(), p.getSubproductoId().getComision()),
                    this.obtenerDamas(p.getDetallePedidoId(), p.getCantidad(), p.getSubproductoId().getComision())))
                    .collect(Collectors.toList()));
            jpedidos.setObservacion(ped.getObservacion());
            respuesta.add(jpedidos);
        }
        return respuesta;
    }
    
    private List<JsonPedidoDama> obtenerDamas(Integer detallePedidoId, Integer cantidad, Double comision){
        Integer totalRegistros = pedidoDamaService.findByDetallePedidoId(detallePedidoId).size();
        if(totalRegistros == 0){
            return new ArrayList<>();
        } else {
            return pedidoDamaService.findByDetallePedidoId(detallePedidoId).stream().map(d -> 
                    new JsonPedidoDama(d.getPedidoDamaId(), d.getDamaId().getDamaId(), d.getDetallePedidoId().getDetallePedidoId(),
                       d.getDamaId().getNombre() + " - " + String.valueOf((cantidad * comision)/totalRegistros), (cantidad * comision)/totalRegistros))
                    .collect(Collectors.toList());
        }
        
    }

    @Override
    public List<JsonPedidosRespuesta> findByTipoPagoId(Integer tipoPagoId) {
        List<Pedido> pedidosList = pedidoRepository.findByTipoPagoId(tipoPagoId);
        List<JsonPedidosRespuesta> respuesta = new ArrayList<>();
        JsonPedidosRespuesta jpedidos;
        
        for(Pedido ped : pedidosList){
            jpedidos = new JsonPedidosRespuesta();
            jpedidos.setPedidoId(ped.getPedidoId());
            if(ped.getClienteId() != null){
                jpedidos.setClienteId(ped.getClienteId().getClienteId());
                jpedidos.setNombreCliente(ped.getClienteId().getNombre());
            }
            if(ped.getMesaId() != null){
                jpedidos.setMesaId(ped.getMesaId().getMesaId());
                jpedidos.setNumeroMesa(ped.getMesaId().getNumero());
            }
            if(ped.getUsuarioId() != null){
                jpedidos.setUsuarioId(ped.getUsuarioId().getUsuarioId());
                jpedidos.setUsername(ped.getUsuarioId().getUsername());
            }
            jpedidos.setNumeroPedido(ped.getNumeroPedido());
            jpedidos.setMontoTotal(ped.getMontoTotal());
            jpedidos.setMontoEntregado(ped.getMontoEntregado());
            jpedidos.setTipoPagoId(ped.getTipoPagoId());
            jpedidos.setFechaRegistro(ped.getFechaRegistro());
            jpedidos.setDetalle(ped.getDetallePedidoList().stream().map(p -> new JsonDetallePedido(p.getDetallePedidoId(), 
                    p.getCantidad(), p.getPrecioUnitario(), p.getPedidoId().getPedidoId(), 
                    p.getSubproductoId().getSubproductoId(), p.getSubproductoId().getNombre(), 
                    p.getUsuarioId().getUsuarioId())).collect(Collectors.toList()));
            jpedidos.setObservacion(ped.getObservacion());
            respuesta.add(jpedidos);
        }
        return respuesta;
    }

    @Override
    public void guardarPedido(JsonPedidos jpedido) {
        JsonRespuestaPedido respuesta = new JsonRespuestaPedido();
        DetallePedido detalle;
        PedidosDamas pedidoDama;
        Pedido pedido = new Pedido();
        
        // registro pedido
        pedido.setFechaRegistro(new Date());
        pedido.setEstadoId(ConstEstados.ACTIVO);
        pedido.setClienteId(clienteService.findById(0).get());
        //pedido.setMesaId(mesaService.findById(objPedidos.getMesaId()).get());
        pedido.setUsuarioId(usuarioService.findById(jpedido.getUsuarioId()).get());
        pedido.setEstadoPedidoId(ConstEstadoPedido.GUARDADO);
        pedido.setNumeroPedido((pedidoRepository.obtenerMaximo()+1));
        pedido.setMontoTotal(jpedido.getMontoTotal());
        pedido.setMontoEntregado(jpedido.getMontoEntregado());
        pedido.setTipoPagoId(jpedido.getTipoPagoId());
        
        pedidoRepository.save(pedido);
        
        // registro detalle
        for(JsonSubproducto subpro : jpedido.getSubproductos()){
            detalle = new DetallePedido();
            detalle.setPedidoId(pedido);
            detalle.setUsuarioId(pedido.getUsuarioId());
            detalle.setSubproductoId(subproductoService.findById(subpro.getSubproductoId()).get());
            detalle.setCantidad(subpro.getCantidad());
            detalle.setPrecioUnitario(subpro.getPrecio());
            detalle.setFechaRegistro(new Date());
            detalle.setEstadoId(ConstEstados.ACTIVO);
            
            detalleService.insertOrUpdate(detalle);
            
            for(JsonPedidoDama pedDama : subpro.getPedidoDamas()){
                pedidoDama = new PedidosDamas();
                pedidoDama.setDamaId(damaService.findById(pedDama.getDamaId()).get());
                pedidoDama.setDetallePedidoId(detalle);
                pedidoDama.setFechaRegistro(new Date());
                pedidoDama.setEstadoId(ConstEstados.ACTIVO);
                pedidoDamaService.insertOrUpdate(pedidoDama);
            }
        }
        
        // 
        Optional<ComandasMeseros> comandaVig = comandaMeseroService.findAbiertoByUsuarioId(jpedido.getUsuarioId());
        ComandasMeseros comanda;
        if(comandaVig.isPresent()){
            comanda = comandaVig.get();
        } else {
            comanda = new ComandasMeseros();
            comanda.setUsuarioId(pedido.getUsuarioId());
            comanda.setEstadoRegistroId(ConstEstadoInventario.ABIERTO);
            comanda.setFechaRegistro(new Date());
            comanda.setEstadoId(ConstEstados.ACTIVO);
            comandaMeseroService.insertOrUpdate(comanda);
        }
        DetalleComandas detalleCom = new DetalleComandas();
        detalleCom.setComandaMeseroId(comanda);
        detalleCom.setPedidoId(pedido);
        detalleCom.setEstadoRegistroId(ConstEstadoPedido.SOLICITADO);
        detalleCom.setFechaRegistro(new Date());
        detalleCom.setEstadoId(ConstEstados.ACTIVO);
        detalleComandoService.insertOrUpdate(detalleCom);
        
        respuesta.setNumeroPedido(pedido.getNumeroPedido());
        respuesta.setMensaje("Solicitud realizada satisfactoriamente.");
        
        //return respuesta;
    }
    
    @Override
    public void agregarPedido(JsonPedidos jpedido) {
        DetallePedido detalle;
        PedidosDamas pedidoDama;
        Pedido pedido = findById(jpedido.getPedidoId()).get();
        pedido.setMontoTotal(jpedido.getMontoTotal());
        pedidoRepository.save(pedido);
        // registro detalle
        for(JsonSubproducto subpro : jpedido.getSubproductos()){
            detalle = new DetallePedido();
            detalle.setPedidoId(pedido);
            detalle.setUsuarioId(pedido.getUsuarioId());
            detalle.setSubproductoId(subproductoService.findById(subpro.getSubproductoId()).get());
            detalle.setCantidad(subpro.getCantidad());
            detalle.setPrecioUnitario(subpro.getPrecio());
            detalle.setFechaRegistro(new Date());
            detalle.setEstadoId(ConstEstados.ACTIVO);
            
            detalleService.insertOrUpdate(detalle);
            
            for(JsonPedidoDama pedDama : subpro.getPedidoDamas()){
                pedidoDama = new PedidosDamas();
                pedidoDama.setDamaId(damaService.findById(pedDama.getDamaId()).get());
                pedidoDama.setDetallePedidoId(detalle);
                pedidoDama.setFechaRegistro(new Date());
                pedidoDama.setEstadoId(ConstEstados.ACTIVO);
                pedidoDamaService.insertOrUpdate(pedidoDama);
            }
        }
    }

    @Override
    public Optional<Pedido> findByUsuarioIdEstadoPedidoId(Integer usuarioId) {
        return pedidoRepository.findByUsuarioIdEstadoPedidoId(usuarioId);
    }
    
    @Override
    public void guardarPedidoVasoChica(JsonPedidos jpedido) {
        JsonRespuestaPedido respuesta = new JsonRespuestaPedido();
        DetallePedido detalle;
        PedidosDamas pedidoDama;
        Pedido pedido = new Pedido();
        
        // registro pedido
        pedido.setFechaRegistro(new Date());
        pedido.setEstadoId(ConstEstados.ACTIVO);
        pedido.setClienteId(clienteService.findById(0).get());
        //pedido.setMesaId(mesaService.findById(objPedidos.getMesaId()).get());
        pedido.setUsuarioId(usuarioService.findById(jpedido.getUsuarioId()).get());
        pedido.setEstadoPedidoId(ConstEstadoPedido.GUARDADO);
        pedido.setNumeroPedido((pedidoRepository.obtenerMaximo()+1));
        pedido.setMontoTotal(jpedido.getMontoTotal());
        pedido.setMontoEntregado(jpedido.getMontoEntregado());
        pedido.setTipoPagoId(jpedido.getTipoPagoId());
        
        pedidoRepository.save(pedido);
        
        // registro detalle
        for(JsonSubproducto subpro : jpedido.getSubproductos()){
            detalle = new DetallePedido();
            detalle.setPedidoId(pedido);
            detalle.setUsuarioId(pedido.getUsuarioId());
            detalle.setSubproductoId(subproductoService.findById(subpro.getSubproductoId()).get());
            detalle.setCantidad(subpro.getCantidad());
            detalle.setPrecioUnitario(subpro.getPrecio());
            detalle.setFechaRegistro(new Date());
            detalle.setEstadoId(ConstEstados.ACTIVO);
            
            detalleService.insertOrUpdate(detalle);
            
            pedidoDama = new PedidosDamas();
            pedidoDama.setDamaId(damaService.findById(subpro.getDamaId()).get());
            pedidoDama.setDetallePedidoId(detalle);
            pedidoDama.setFechaRegistro(new Date());
            pedidoDama.setEstadoId(ConstEstados.ACTIVO);
            pedidoDamaService.insertOrUpdate(pedidoDama);
        }
        
        // 
        Optional<ComandasMeseros> comandaVig = comandaMeseroService.findAbiertoByUsuarioId(jpedido.getUsuarioId());
        ComandasMeseros comanda;
        if(comandaVig.isPresent()){
            comanda = comandaVig.get();
        } else {
            comanda = new ComandasMeseros();
            comanda.setUsuarioId(pedido.getUsuarioId());
            comanda.setEstadoRegistroId(ConstEstadoInventario.ABIERTO);
            comanda.setFechaRegistro(new Date());
            comanda.setEstadoId(ConstEstados.ACTIVO);
            comandaMeseroService.insertOrUpdate(comanda);
        }
        DetalleComandas detalleCom = new DetalleComandas();
        detalleCom.setComandaMeseroId(comanda);
        detalleCom.setPedidoId(pedido);
        detalleCom.setEstadoRegistroId(ConstEstadoPedido.SOLICITADO);
        detalleCom.setFechaRegistro(new Date());
        detalleCom.setEstadoId(ConstEstados.ACTIVO);
        detalleComandoService.insertOrUpdate(detalleCom);
        
        respuesta.setNumeroPedido(pedido.getNumeroPedido());
        respuesta.setMensaje("Solicitud realizada satisfactoriamente.");
        
    }

    @Override
    public void agregarPedidoVasoChica(JsonPedidos jpedido) {
        DetallePedido detalle;
        PedidosDamas pedidoDama;
        Pedido pedido = findById(jpedido.getPedidoId()).get();
        pedido.setMontoTotal(jpedido.getMontoTotal());
        pedidoRepository.save(pedido);
        
        // registro detalle
        for(JsonSubproducto subpro : jpedido.getSubproductos()){
            detalle = new DetallePedido();
            detalle.setPedidoId(pedido);
            detalle.setUsuarioId(pedido.getUsuarioId());
            detalle.setSubproductoId(subproductoService.findById(subpro.getSubproductoId()).get());
            detalle.setCantidad(subpro.getCantidad());
            detalle.setPrecioUnitario(subpro.getPrecio());
            detalle.setFechaRegistro(new Date());
            detalle.setEstadoId(ConstEstados.ACTIVO);
            
            detalleService.insertOrUpdate(detalle);
            
            pedidoDama = new PedidosDamas();
            pedidoDama.setDamaId(damaService.findById(subpro.getDamaId()).get());
            pedidoDama.setDetallePedidoId(detalle);
            pedidoDama.setFechaRegistro(new Date());
            pedidoDama.setEstadoId(ConstEstados.ACTIVO);
            pedidoDamaService.insertOrUpdate(pedidoDama);
        }
    }

    @Override
    public void consolidarPedido(JsonPedidos jpedido) {
        Pedido pedido = findById(jpedido.getPedidoId()).get();
        pedido.setEstadoPedidoId(ConstEstadoPedido.SOLICITADO);
        pedidoRepository.save(pedido);
    }

    @Override
    public void pagarPedido(JsonPedidos jpedido) {
        Pedido pedido = findById(jpedido.getPedidoId()).get();
        pedido.setUsuarioDespachaId(usuarioService.findById(jpedido.getUsuarioId()).get());
        pedido.setFechaPagoCuenta(new Date());
        pedido.setMontoEntregado(jpedido.getMontoEntregado());
        pedido.setTipoPagoId(ConstTipoPagoID.AL_CONTADO);
        pedidoRepository.save(pedido);
    }

    @Override
    public void imprimirPedido(JsonPedidos jpedido) {
        int ancho_cantidad = 9;
        int ancho_descripcion = 28;
        int ancho_total = 9;
        Pedido pedido = findById(jpedido.getPedidoId()).get();
        String seg_linea = "";
        String cadena_impresion = "Garzon: " + pedido.getUsuarioId().getUsername();
        cadena_impresion = String.format("%-20s", cadena_impresion);
        cadena_impresion += " Pedido: " + pedido.getNumeroPedido();
        seg_linea = "Fecha: " + OperacionesFecha.ConvertirDateToString(pedido.getFechaRegistro());
        cadena_impresion += "\n" + String.format("%-20s", seg_linea) +
                        " Hora: " + OperacionesFecha.ConvertirHoraToString(pedido.getFechaRegistro());
        
        
    }
}