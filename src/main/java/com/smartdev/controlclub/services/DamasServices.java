/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Damas;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class DamasServices implements DamasInterfaces {
    
    @Autowired
    private DamasRepository damaRepository;

    @Override
    public List<Damas> findByActives() {
        return damaRepository.findByActives();
    }

    @Override
    public List<Damas> findByHabilitado(boolean habilitada) {
        return damaRepository.findByHabilitado(habilitada);
    }

    @Override
    public Optional<Damas> findById(Integer id) {
        return damaRepository.findById(id);
    }

    @Override
    public void cambiarEstado(Integer damaId, boolean estado) {
        Damas dama = findById(damaId).get();
        dama.setHabilitada(estado);
        damaRepository.save(dama);
    }

    @Override
    public void insertOrUpdate(Damas dama) {
        damaRepository.save(dama);
    }

    @Override
    public Optional<Damas> findByCodigo(String codigo) {
        return damaRepository.findByCodigo(codigo);
    }
    
}
