/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Cliente;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface ClienteInterfaces {
    
    List<Cliente> findByActives();
    Optional<Cliente> findById(Integer id);
    void insertOrUpdate(Cliente cliente);
    void delete(Cliente cliente);
    Optional<Cliente> findByDocumentoIdentidad(String nit);
}
