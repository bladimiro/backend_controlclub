/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.PedidosDamas;
import com.smartdev.controlclub.model.JsonPedidoDama;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class PedidosDamasServices implements PedidosDamasInterfaces {

    @Autowired
    private PedidosDamasRepository pedidoDamaRepository;
    
    @Autowired
    private PedidosDamasServices pedidoDamaService;
    
    @Override
    public List<PedidosDamas> findByEstadoId(Integer estadoId) {
        return pedidoDamaRepository.findByEstadoId(estadoId);
    }

    @Override
    public Optional<PedidosDamas> findById(Integer id) {
        return pedidoDamaRepository.findById(id);
    }

    @Override
    public void insertOrUpdate(PedidosDamas pedidoDama) {
        pedidoDamaRepository.save(pedidoDama);
    }

    @Override
    public List<PedidosDamas> findByDetallePedidoId(Integer detallePedidoId) {
        return pedidoDamaRepository.findByDetallePedidoId(detallePedidoId);
    }

    @Override
    public List<JsonPedidoDama> findByDamaIdFecha(Integer damaId, Date fechaInicio, Date fechaFin) {
        List<PedidosDamas> pedidosList = pedidoDamaRepository.findByDamaIdFecha(damaId, fechaInicio, fechaFin);
        List<JsonPedidoDama> resultados = new ArrayList<>();
        Integer totalRegistros = 0;
        JsonPedidoDama pedido;
        for(PedidosDamas ped : pedidosList){
            pedido = new JsonPedidoDama();
            totalRegistros = pedidoDamaService.findByDetallePedidoId(ped.getDetallePedidoId().getDetallePedidoId()).size();
            pedido.setDamaId(ped.getDamaId().getDamaId());
            pedido.setNombreDama(ped.getDamaId().getNombre());
            pedido.setComision((ped.getDetallePedidoId().getCantidad() * ped.getDetallePedidoId().getSubproductoId().getComision())/totalRegistros);
            pedido.setCantidad(ped.getDetallePedidoId().getCantidad());
            pedido.setNombreProducto(ped.getDetallePedidoId().getSubproductoId().getNombre());
            pedido.setFechaRegistro(ped.getFechaRegistro());
            pedido.setNumeroPedido(ped.getDetallePedidoId().getPedidoId().getNumeroPedido());
            resultados.add(pedido);
        }
        return resultados;
    }
    
}
