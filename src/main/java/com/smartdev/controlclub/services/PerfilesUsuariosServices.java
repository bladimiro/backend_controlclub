/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Opciones;
import com.smartdev.controlclub.entities.PerfilesUsuarios;
import com.smartdev.controlclub.model.JsonOpciones;
import com.smartdev.controlclub.model.JsonPerfilesUsuarios;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class PerfilesUsuariosServices implements PerfilesUsuariosInterfaces {

    @Autowired
    private PerfilesUsuariosRepository perfUsuarioRepository;
    
    @Autowired
    private OpcionesServices opcionService;
    
    @Override
    public List<JsonOpciones> findOpcionesPrincipalesByUsuarioId(Integer usuarioId) {
        List<Object[]> opcionesPrales = perfUsuarioRepository.findOpcionesPrincipalesByUsuarioId(usuarioId);
        List<JsonOpciones> respuesta = new ArrayList<>();
        JsonOpciones opcion;
        
        for(Object[] obj : opcionesPrales){
            opcion = new JsonOpciones();
            opcion.setOpcionId(new Integer(obj[0].toString()));
            opcion.setNombre(obj[1].toString());
            opcion.setSubopciones(opcionService.findByOpcionReferenciaIdUsuarioId(usuarioId, opcion.getOpcionId()));
            respuesta.add(opcion);
        }
        return respuesta;
    }

    @Override
    public Optional<PerfilesUsuarios> findById(Integer perfilId) {
        return perfUsuarioRepository.findById(perfilId);
    }

    @Override
    public void insertOrUpdate(PerfilesUsuarios perfilUsuario) {
        perfUsuarioRepository.save(perfilUsuario);
    }

    @Override
    public void delete(PerfilesUsuarios perfilUsuario) {
        perfilUsuario.setEstadoId(ConstEstados.INACTIVO);
        perfUsuarioRepository.save(perfilUsuario);
    }

    @Override
    public List<JsonPerfilesUsuarios> findPerfilesByUsuarioId(Integer usuarioId) {
        return perfUsuarioRepository.findPerfilesByUsuarioId(usuarioId);
    }
    
}
