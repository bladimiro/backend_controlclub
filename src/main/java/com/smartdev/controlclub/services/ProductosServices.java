/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.*;
import com.smartdev.controlclub.model.JsonProveedoresProducto;
import com.smartdev.controlclub.model.JsonRespuestaGeneral;
import com.smartdev.controlclub.utils.ConstEstadoInventario;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class ProductosServices implements ProductosInterfaces {

    @Autowired
    private ProductosRepository productoRepository;
    
    @Autowired
    private InventariosServices inventarioService;
    
    @Autowired
    private DetalleInventarioServices detalleService;
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @Autowired
    private ProveedoresProductoServices provProductoService;
    
    @Override
    public List<Producto> findAllActives() {
        return productoRepository.findAllActives();
    }

    @Override
    public Optional<Producto> findById(Integer productoId) {
        return productoRepository.findById(productoId);
    }

    @Override
    public void insertOrUpdate(Producto producto) {
        boolean registrarInventario = false;
        if(producto.getProductoId() == null) registrarInventario = true;
        productoRepository.save(producto);
        
        if(registrarInventario){
            Optional<Inventario> invenOpt = inventarioService.findByEstadoInventario(ConstEstadoInventario.ABIERTO);
            Inventario inventario;
            if(invenOpt.isPresent()){
                inventario = invenOpt.get();
            } else {
                inventario = new Inventario();
                inventario.setUsuarioId(producto.getUsuarioId());
                inventario.setFechaInicio(new Date());
                inventario.setEstadoInventarioId(ConstEstadoInventario.ABIERTO);
                inventario.setFechaRegistro(new Date());
                inventario.setEstadoId(ConstEstados.ACTIVO);
                inventarioService.insertOrUpdate(inventario);
            }
            
            DetalleInventario detalleInven = new DetalleInventario();
            detalleInven.setInventarioId(inventario);
            detalleInven.setProductoId(producto);
            detalleInven.setUsuarioId(producto.getUsuarioId());
            detalleInven.setCantidadDisponible(0D);
            detalleInven.setCantidadUsada(0D);
            detalleInven.setFechaRegistro(new Date());
            detalleInven.setEstadoId(ConstEstados.ACTIVO);
            
            detalleService.insertOrUpdate(detalleInven);
        }
        
    }

    @Override
    public void delete(Producto producto) {
        producto.setEstadoId(ConstEstados.INACTIVO);
        productoRepository.save(producto);
    }

    @Override
    public JsonRespuestaGeneral existeRelacion(Integer productoId) {
        JsonRespuestaGeneral respuesta = new JsonRespuestaGeneral();
        
        List<JsonProveedoresProducto> relacionProveedor = provProductoService.findByProductoId(productoId);
        if(!relacionProveedor.isEmpty()){
            
        }
        return respuesta;
    }    
}
