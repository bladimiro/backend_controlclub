/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.TipoGasto;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bladimir
 */
@Service
public class TiposGastosServices implements TiposGastosInterfaces {
    
    @Autowired
    private TiposGastosRepository tipoGastoRepository;

    @Override
    public List<TipoGasto> findAllActivos() {
        return tipoGastoRepository.findAllActivos();
    }

    @Override
    public Optional<TipoGasto> findById(Integer id) {
        return tipoGastoRepository.findById(id);
    }

    @Override
    public void deleteById(Integer id) {
        TipoGasto tipoGasto = findById(id).get();
        tipoGasto.setEstadoId(ConstEstados.INACTIVO);
        tipoGastoRepository.save(tipoGasto);
    }

    @Override
    public void insertOrUpdate(TipoGasto tipoGasto) {
        tipoGastoRepository.save(tipoGasto);
    }
    
}
