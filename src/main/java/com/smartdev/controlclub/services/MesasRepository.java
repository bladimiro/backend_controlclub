/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Mesa;
import com.smartdev.controlclub.model.JsonMesa;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface MesasRepository extends JpaRepository<Mesa, Integer>{
    
    @Query("select new com.smartdev.controlclub.model.JsonMesa(m.mesaId, m.salaId.salaId, m.numero) " +
            "from Mesa m where m.estadoId=1000 ")
    List<JsonMesa> findAllActives();
    
    @Query("select new com.smartdev.controlclub.model.JsonMesa(m.mesaId, m.salaId.salaId, m.numero) " +
            "from Mesa m where m.estadoId=1000 and m.salaId.salaId = :salaId")
    List<JsonMesa> findBySalaId(@Param("salaId") Integer salaId);
}
