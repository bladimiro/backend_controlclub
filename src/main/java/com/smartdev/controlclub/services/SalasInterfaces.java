/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Sala;
import com.smartdev.controlclub.model.JsonSala;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface SalasInterfaces {
    List<JsonSala> findByEmpresaId(Integer empresaId);
    Optional<Sala> findById(Integer salaId);
    void insertOrUpdate(Sala sala);
    void delete(Sala sala);
}
