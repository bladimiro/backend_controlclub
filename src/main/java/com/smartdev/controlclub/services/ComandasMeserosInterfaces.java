/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.ComandasMeseros;
import com.smartdev.controlclub.model.JsonComandasMeseros;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface ComandasMeserosInterfaces {
    Optional<ComandasMeseros> findAbiertoByUsuarioId(Integer usuarioId);
    List<ComandasMeseros> findCerradoByEstadoRegistroId(Integer usuarioId, Integer estadoRegistroId);
    List<ComandasMeseros> findByEstadoRegistroIdFechas(Integer estadoRegistroId, Date fechaInicio, Date fechaFin);
    void insertOrUpdate(ComandasMeseros comandaMesero);
    Optional<ComandasMeseros> findById(Integer id);
    void cerrarComandaMesero(Integer usuarioId);
    List<JsonComandasMeseros> obtenerTotalesComandasByFecha(Date fecha);
    Double obtenerTotal(Date fecha);
}
