/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Perfiles;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class PerfilesServices implements PerfilesInterfaces {
    
    @Autowired
    private PerfilesRepository perfilRepository;

    @Override
    public List<Perfiles> findByActives() {
        return perfilRepository.findByActives();
    }

    @Override
    public Optional<Perfiles> findById(Integer perfilId) {
        return perfilRepository.findById(perfilId);
    }
    
}
