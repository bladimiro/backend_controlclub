/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleComandas;
import com.smartdev.controlclub.model.JsonDetalleComanda;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface DetalleComandasRepository extends JpaRepository<DetalleComandas, Integer>{
    /*@Query("select new com.smartdev.controlclub.model.JsonDetalleComanda(d.detalleComandaId, " +
            "d.comandaMeseroId.comandaMeseroId, d.pedidoId.pedidoId, d.pedidoId.numeroPedido) " +
            "from DetalleComandas d where d.estadoId=1000 and d.comandaMeseroId.comandaMeseroId = :comandaMeseroId")*/
    @Query("select d from DetalleComandas d where d.estadoId=1000 and " +
            "d.comandaMeseroId.comandaMeseroId = :comandaMeseroId")
    List<DetalleComandas> findByComandaMeseroId(@Param("comandaMeseroId") Integer comandaMeseroId);
    
    @Query("select d from DetalleComandas d where d.estadoId=1000 and " +
            "date(d.comandaMeseroId.fechaRegistro) = :fecha and " +
            "d.comandaMeseroId.usuarioId.usuarioId = :usuarioId ")
    List<DetalleComandas> findByUsuarioIdFecha(@Param("usuarioId") Integer usuarioId, @Param("fecha") Date fecha);
}
