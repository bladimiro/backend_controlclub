/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleInventario;
import com.smartdev.controlclub.entities.Inventario;
import com.smartdev.controlclub.model.JsonDetalleInventario;
import com.smartdev.controlclub.model.JsonInventario;
import com.smartdev.controlclub.utils.ConstEstadoInventario;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class InventariosServices implements InventariosInterfaces {

    @Autowired
    private InventariosRepository inventarioRepository;
    
    @Autowired
    private DetalleInventarioServices detalleInventarioService;
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @Autowired
    private ProductosServices productoService;
    
    @Override
    public Optional<Inventario> findByEstadoInventario(Integer estadoInventarioId) {
        return inventarioRepository.findByEstadoInventario(estadoInventarioId);
    }

    @Override
    public Optional<Inventario> findById(Integer inventarioId) {
        return inventarioRepository.findById(inventarioId);
    }

    @Override
    public void insertOrUpdate(Inventario inventario) {
        inventarioRepository.save(inventario);
    }

    @Override
    public List<JsonInventario> findByEstadoHistorico() {
        return inventarioRepository.findByEstadoHistorico();
    }

    @Override
    public JsonInventario findDetalleById(Integer inventarioId) {
        Inventario inven = inventarioRepository.findById(inventarioId).get();
        JsonInventario respuesta = new JsonInventario();
        respuesta.setInventarioId(inven.getInventarioId());
        respuesta.setEstadoInventarioId(inven.getEstadoInventarioId());
        respuesta.setFechaInicio(inven.getFechaInicio());
        respuesta.setFechaCierre(inven.getFechaCierre());
        respuesta.setDetalleInventario(detalleInventarioService.findByInventarioId(inventarioId).stream()
                .map(d -> new JsonDetalleInventario(d.getDetalleInventarioId(), d.getCantidadDisponible(), 
                        d.getCantidadUsada(), d.getInventarioId().getInventarioId(), d.getProductoId().getProductoId(), 
                        d.getProductoId().getNombre())).collect(Collectors.toList()));
        return respuesta;
    }

    @Override
    public void cerrarInventario(Inventario inventario, Integer usuarioId) {
        inventario.setUsuarioCierreId(usuarioService.findById(usuarioId).get());
        inventario.setEstadoInventarioId(ConstEstadoInventario.CERRADO);
        inventario.setFechaCierre(new Date());
        inventarioRepository.save(inventario);
        
        Inventario nuevo = new Inventario();
        nuevo.setEstadoInventarioId(ConstEstadoInventario.ABIERTO);
        nuevo.setUsuarioId(inventario.getUsuarioCierreId());
        nuevo.setFechaInicio(new Date());
        nuevo.setFechaRegistro(new Date());
        nuevo.setEstadoId(ConstEstados.ACTIVO);
        inventarioRepository.save(nuevo);
        
        DetalleInventario detalle;
        List<DetalleInventario> detalleList = detalleInventarioService.findByInventarioId(inventario.getInventarioId());
        
        for(DetalleInventario deta : detalleList){
            detalle = new DetalleInventario();
            detalle.setInventarioId(nuevo);
            detalle.setProductoId(deta.getProductoId());
            detalle.setUsuarioId(inventario.getUsuarioCierreId());
            detalle.setCantidadDisponible(deta.getCantidadDisponible() - deta.getCantidadUsada());
            detalle.setCantidadUsada(0D);
            detalle.setFechaRegistro(new Date());
            detalle.setEstadoId(ConstEstados.ACTIVO);
            
            detalleInventarioService.insertOrUpdate(detalle);
        }
    }
    
}
