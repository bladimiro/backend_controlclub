/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetallePedido;
import com.smartdev.controlclub.model.JsonDetallePedido;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface DetallePedidoInterfaces {
    List<JsonDetallePedido> findByPedidoId(Integer pedidoId);
    Optional<DetallePedido> findById(Integer detalleId);
    void insertOrUpdate(DetallePedido detalle);
    void delete(DetallePedido detalle);
}
