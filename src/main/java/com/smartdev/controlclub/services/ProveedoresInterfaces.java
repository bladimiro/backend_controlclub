/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Proveedor;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface ProveedoresInterfaces {
    
    List<Proveedor> findAllActives();
    Optional<Proveedor> findById(Integer id);
    void insertOrUpdate(Proveedor proveedor);
    void delete(Proveedor proveedor);
    
}
