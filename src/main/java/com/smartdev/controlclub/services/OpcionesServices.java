/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.model.JsonOpciones;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class OpcionesServices implements OpcionesInterfaces {

    @Autowired
    private OpcionesRepository opcionRepository;
    
    @Override
    public List<JsonOpciones> findByOpcionRefenciaId(Integer opcionId) {
        return opcionRepository.findByOpcionRefenciaId(opcionId);
    }

    @Override
    public List<JsonOpciones> findByOpcionReferenciaIdUsuarioId(Integer usuarioId, Integer referenciaId) {
        List<Object[]> opcionListObj = opcionRepository.findByOpcionReferenciaIdUsuarioId(usuarioId, referenciaId);
        return opcionListObj.stream()
                .map(p -> new JsonOpciones(new Integer(p[0].toString()), p[1].toString(), p[2].toString(), 
                new Integer(p[3].toString()))).collect(Collectors.toList());
    }
    
}
