/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.PedidosDamas;
import com.smartdev.controlclub.model.JsonPedidoDama;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface PedidosDamasInterfaces {
    List<PedidosDamas> findByEstadoId(Integer estadoId);
    Optional<PedidosDamas> findById(Integer id);
    void insertOrUpdate(PedidosDamas pedidoDama);
    List<PedidosDamas> findByDetallePedidoId(Integer detallePedidoId);
    List<JsonPedidoDama> findByDamaIdFecha(Integer damaId, Date fechaInicio, Date fechaFin);
}
