/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.ProveedorProducto;
import com.smartdev.controlclub.model.JsonProveedoresProducto;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface ProveedoresProductoInterface {
    List<JsonProveedoresProducto> findByProductoId(Integer productoId);
    Optional<ProveedorProducto> findById(Integer provProductoId);
    void insertOrUpdate(ProveedorProducto proveedorProducto);
    void delete(ProveedorProducto proveedorProducto);
    List<JsonProveedoresProducto> findByActive();
}
