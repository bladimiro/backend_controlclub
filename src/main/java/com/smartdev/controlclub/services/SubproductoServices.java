/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Subproducto;
import com.smartdev.controlclub.model.JsonSubproducto;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class SubproductoServices implements SubproductoInterfaces{
    
    @Autowired
    private SubproductoRepository subprodRepository;

    @Override
    public List<JsonSubproducto> findAllActives() {
        return subprodRepository.findAllActives();
    }

    @Override
    public List<JsonSubproducto> findByProductoId(Integer productoId) {
        return subprodRepository.findByProductoId(productoId);
    }

    @Override
    public Optional<Subproducto> findById(Integer productoId) {
        return subprodRepository.findById(productoId);
    }

    @Override
    public void insertOrUpdate(Subproducto subproducto) {
        subprodRepository.save(subproducto);
    }

    @Override
    public void delete(Subproducto subproducto) {
        subproducto.setEstadoId(ConstEstados.INACTIVO);
        subprodRepository.save(subproducto);
    }

    @Override
    public List<JsonSubproducto> findByTipoIdTiSolicitante(Integer tipoId, Integer tipoSolicitudId) {
        return subprodRepository.findByTipoIdTiSolicitante(tipoId, tipoSolicitudId);
    }   
    
}