/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetallePedido;
import com.smartdev.controlclub.model.JsonDetallePedido;
import com.smartdev.controlclub.model.JsonPedidoDama;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bladimir
 */
@Service
@Transactional
public class DetallePedidoServices implements DetallePedidoInterfaces {

    @Autowired
    private DetallePedidoRepository detalleRepository;
    
    @Autowired
    private PedidosDamasServices pedidoDamaService;
            
    @Override
    public List<JsonDetallePedido> findByPedidoId(Integer pedidoId) {
        List<DetallePedido> detalles = detalleRepository.findByPedidoId(pedidoId);
        List<JsonDetallePedido> respuesta = new ArrayList<>();
        JsonDetallePedido jdetalle;
        
        for(DetallePedido deta : detalles){
            jdetalle = new JsonDetallePedido();
            jdetalle.setDetallePedidoId(deta.getDetallePedidoId());
            jdetalle.setCantidad(deta.getCantidad());
            jdetalle.setPrecioUnitario(deta.getPrecioUnitario());
            jdetalle.setNombreSubproducto(deta.getSubproductoId().getNombre());
            List<JsonPedidoDama> pedidosDama = pedidoDamaService.findByDetallePedidoId(deta.getDetallePedidoId()).stream().map(d -> 
                    new JsonPedidoDama(d.getPedidoDamaId(), d.getPedidoDamaId(), d.getDetallePedidoId().getDetallePedidoId(),
                       d.getDamaId().getNombre())).collect(Collectors.toList());
            jdetalle.setPedidosDama(pedidosDama);
            if(pedidosDama.isEmpty())
                jdetalle.setDamasConcatenadas("");
            else {
                String concatenado = " (";
                String separador = "";
                for(JsonPedidoDama jped : pedidosDama) {
                    concatenado += separador + jped.getNombreDama();
                    separador = ", ";
                }
                concatenado += ")";
                jdetalle.setDamasConcatenadas(concatenado);
            }
            respuesta.add(jdetalle);
        }
        return respuesta;
    }

    @Override
    public Optional<DetallePedido> findById(Integer detalleId) {
        return detalleRepository.findById(detalleId);
    }

    @Override
    public void insertOrUpdate(DetallePedido detalle) {
        detalleRepository.save(detalle);
    }

    @Override
    public void delete(DetallePedido detalle) {
        detalle.setEstadoId(ConstEstados.INACTIVO);
        detalleRepository.save(detalle);
    }
    
}
