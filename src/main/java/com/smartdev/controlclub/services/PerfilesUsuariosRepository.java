/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.PerfilesUsuarios;
import com.smartdev.controlclub.model.JsonPerfilesUsuarios;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface PerfilesUsuariosRepository extends JpaRepository<PerfilesUsuarios, Integer>{
    
    @Query("select p from PerfilesUsuarios p where p.estadoId=1000")
    List<PerfilesUsuarios> findByActives();
    
    @Query("select new com.smartdev.controlclub.model.JsonPerfilesUsuarios(p.perfilUsuarioId, " + 
            "p.usuarioId.usuarioId, p.perfilId.perfilId, p.perfilId.nombre) " +
            "from PerfilesUsuarios p where p.estadoId=1000 " +
            "and p.usuarioId.usuarioId = :usuarioId ")
    List<JsonPerfilesUsuarios> findPerfilesByUsuarioId(Integer usuarioId);
    
    @Query(value = "select distinct op1.opcion_id, op1.nombre " +
            "from perfiles_usuarios pu " +
            "inner join perfiles_opciones po " +
            "on pu.perfil_id = po.perfil_id " +
            "inner join opciones op " +
            "on op.opcion_id = po.opcion_id " +
            "inner join opciones op1 " +
            "on op.opcion_ref_id = op1.opcion_id " +
            "where pu.estado_id=1000 " +
            "and po.estado_id=1000 " +
            "and op.estado_id=1000 " +
            "and pu.usuario_id=:usuarioId " +
            "order by op1.opcion_id ", 
            nativeQuery = true)
    List<Object[]> findOpcionesPrincipalesByUsuarioId(@Param("usuarioId") Integer usuarioId);
}
