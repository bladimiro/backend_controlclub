/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Movimiento;
import com.smartdev.controlclub.model.JsonMovimiento;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bladimir
 */
@Repository
public interface MovimientoRepository extends JpaRepository<Movimiento, Integer>{
    
    @Query("select m from Movimiento m where m.estadoId=1000")
    List<Movimiento> findByActives();
    
    /*new com.smartdev.controlclub.model.JsonMovimiento(m.movimientoId, " +
            "m.tipoId, m.monto, m.fechaMovimiento, m.glosa)*/
    @Query("select m from Movimiento m where m.estadoId=1000 and " +
            "(date(m.fechaMovimiento) between :fechaInicio and :fechaFin) ")
    List<Movimiento> findByFechas(@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
    
    @Query("select m from Movimiento m where m.estadoId=1000 and m.gastoId.gastoId = :gastoId")
    Optional<Movimiento> findByGastoId(@Param("gastoId") Integer gastoId);
    
    @Query("select m from Movimiento m where m.estadoId=1000 and m.pedidoId.pedidoId = :pedidoId")
    Optional<Movimiento> findByPedidoId(@Param("pedidoId") Integer pedidoId);
}
