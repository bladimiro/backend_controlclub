/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.DetalleInventario;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author bladimir
 */
public interface DetalleInventarioInterfaces {
    List<DetalleInventario> findByInventarioId(Integer inventarioId);
    Optional<DetalleInventario> findById(Integer DetalleInventarioId);
    void insertOrUpdate(DetalleInventario detalle);
    void delete(DetalleInventario detalle);
    Optional<DetalleInventario> findByInventarioIdProductoId(Integer inventarioId, Integer productoId);
}
