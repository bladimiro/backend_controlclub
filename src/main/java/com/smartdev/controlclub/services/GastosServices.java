/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Gasto;
import com.smartdev.controlclub.model.JsonGasto;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bladimir
 */
@Service
public class GastosServices implements GastosInterfaces {

    @Autowired
    private GastosRepository gastoRepository;
    
    @Override
    public List<JsonGasto> findAllActivos() {
        return gastoRepository.findAllActivos();
    }

    @Override
    public Optional<Gasto> findById(Integer id) {
        return gastoRepository.findById(id);
    }

    @Override
    public void insertOrUpdate(Gasto gasto) {
        gastoRepository.save(gasto);
    }

    @Override
    public void deleteById(Integer id) {
        Gasto gasto = findById(id).get();
        gasto.setEstadoId(ConstEstados.INACTIVO);
        gastoRepository.save(gasto);
    }

    @Override
    public List<JsonGasto> findByFecha(Date fecha) {
        return gastoRepository.findByFecha(fecha);
    }
    
}
