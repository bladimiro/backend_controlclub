/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.services;

import com.smartdev.controlclub.entities.Empresa;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bladimir
 */
@Service
public class EmpresaServices implements EmpresaInterfaces {

    @Autowired
    private EmpresaRepository empresaRepository;
    
    @Override
    public Optional<Empresa> findById(Integer id) {
        return empresaRepository.findById(id);
    }

    @Override
    public void insertOrUpdate(Empresa empresa) {
        empresaRepository.save(empresa);
    }

    @Override
    public Empresa findFirst() {
        List<Empresa> empresaList = empresaRepository.findAll();
        if(empresaList.isEmpty())
            return null;
        else
            return empresaList.get(0);
    }
    
}
