/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.utils;

/**
 *
 * @author bladimir
 */
public class ConstEstadoPedido {
    public static Integer SOLICITADO = 1003;
    public static Integer ATENDIDO = 1004;
    public static Integer CANCELADO = 1005;
    public static Integer GUARDADO = 1015;
}
