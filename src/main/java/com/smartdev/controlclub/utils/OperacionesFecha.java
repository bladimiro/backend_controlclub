/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 *
 * @author bladimir
 */
public class OperacionesFecha {
    public static boolean esFinDeSemana(Date fecha1){
        LocalDate fecha = fecha1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        DayOfWeek dayOfWeek = fecha.getDayOfWeek();
        
        if(dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY)
            return true;
        else
            return false;   
    }
    
    public static boolean esFinDeSemana(LocalDate fecha){
        DayOfWeek dayOfWeek = fecha.getDayOfWeek();
        
        if(dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY)
            return true;
        else
            return false;
    }
    
    public static Date convertirFormatoYYYYMMDDHHMM(String fecha) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.parse(fecha);
    }
    
    public static Date convertirFormatoYYYYMMDD(String fecha) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(fecha);
    }
    
    public static Date convertirFormatoDDMMYYYY(String fecha) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(fecha);
    }
    
    public static Date convertirFormatoHHmm(String hora) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.parse(hora);
    }
    
    public static Date sumarDias(Date fecha, Integer numeroDias){
        LocalDate fechaAux = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        fechaAux = fechaAux.plusDays(numeroDias);
        return Date.from(fechaAux.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    
    public static String ConvertirDateToString(Date fecha){
        return new SimpleDateFormat("dd/MM/yyyy").format(fecha);
    }
    
    public static String ConvertirHoraToString(Date fecha){
        return new SimpleDateFormat("HH:mm").format(fecha);
    }
}
