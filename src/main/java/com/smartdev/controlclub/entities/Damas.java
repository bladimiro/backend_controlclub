/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
/**
 *
 * @author bladimir
 */
@Entity
@Table(catalog = "controlclub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Damas.findAll", query = "SELECT d FROM Damas d")})
public class Damas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dama_id", nullable = false)
    private Integer damaId;
    @Column(length = 50)
    private String nombre;
    @Column(length = 20)
    private String codigo;
    private Boolean habilitada;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "estado_id")
    private Integer estadoId;
    @Transient
    @OneToMany(mappedBy = "damaId")
    private List<PedidosDamas> pedidosDamasList;

    public Damas() {
    }

    public Damas(Integer damaId) {
        this.damaId = damaId;
    }

    public Integer getDamaId() {
        return damaId;
    }

    public void setDamaId(Integer damaId) {
        this.damaId = damaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Boolean getHabilitada() {
        return habilitada;
    }

    public void setHabilitada(Boolean habilitada) {
        this.habilitada = habilitada;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public List<PedidosDamas> getPedidosDamasList() {
        return pedidosDamasList;
    }

    public void setPedidosDamasList(List<PedidosDamas> pedidosDamasList) {
        this.pedidosDamasList = pedidosDamasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (damaId != null ? damaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Damas)) {
            return false;
        }
        Damas other = (Damas) object;
        if ((this.damaId == null && other.damaId != null) || (this.damaId != null && !this.damaId.equals(other.damaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.Damas[ damaId=" + damaId + " ]";
    }
    
}