/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author bladimir
 */
@Entity
@Table(catalog = "controlclub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Pedido.findAll", query = "SELECT p FROM Pedido p")})
public class Pedido implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pedido_id", nullable = false)
    private Integer pedidoId;
    @Column(name = "numero_pedido")
    private Integer numeroPedido;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "monto_entregado", precision = 17, scale = 17)
    private Double montoEntregado;
    @Column(name = "monto_total", precision = 17, scale = 17)
    private Double montoTotal;
    @Column(name = "monto_cambio", precision = 17, scale = 17)
    private Double montoCambio;
    @Column(name = "tipo_pago_id")
    private Integer tipoPagoId;
    @Column(name = "fecha_pago_cuenta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPagoCuenta;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "estado_id")
    private Integer estadoId;
    @Column(name = "estado_pedido_id")
    private Integer estadoPedidoId;
    @Size(max = 500)
    @Column(length = 500)
    private String observacion;
    //@Transient
    @OneToMany(mappedBy = "pedidoId")
    private List<DetallePedido> detallePedidoList;
    @Transient
    @OneToMany(mappedBy = "pedidoId")
    private List<Movimiento> movimientoList;
    @JoinColumn(name = "cliente_id", referencedColumnName = "cliente_id")
    @ManyToOne
    private Cliente clienteId;
    @JoinColumn(name = "mesa_id", referencedColumnName = "mesa_id")
    @ManyToOne
    private Mesa mesaId;
    @JoinColumn(name = "usuario_cancela_id", referencedColumnName = "usuario_id")
    @ManyToOne
    private Usuario usuarioCancelaId;
    @JoinColumn(name = "usuario_despacha_id", referencedColumnName = "usuario_id")
    @ManyToOne
    private Usuario usuarioDespachaId;
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id")
    @ManyToOne
    private Usuario usuarioId;

    public Pedido() {
    }

    public Pedido(Integer pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Integer getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Integer pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Integer getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public Double getMontoEntregado() {
        return montoEntregado;
    }

    public void setMontoEntregado(Double montoEntregado) {
        this.montoEntregado = montoEntregado;
    }

    public Double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Double getMontoCambio() {
        return montoCambio;
    }

    public void setMontoCambio(Double montoCambio) {
        this.montoCambio = montoCambio;
    }

    public Integer getTipoPagoId() {
        return tipoPagoId;
    }

    public void setTipoPagoId(Integer tipoPagoId) {
        this.tipoPagoId = tipoPagoId;
    }

    public Date getFechaPagoCuenta() {
        return fechaPagoCuenta;
    }

    public void setFechaPagoCuenta(Date fechaPagoCuenta) {
        this.fechaPagoCuenta = fechaPagoCuenta;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public Integer getEstadoPedidoId() {
        return estadoPedidoId;
    }

    public void setEstadoPedidoId(Integer estadoPedidoId) {
        this.estadoPedidoId = estadoPedidoId;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public List<DetallePedido> getDetallePedidoList() {
        return detallePedidoList;
    }

    public void setDetallePedidoList(List<DetallePedido> detallePedidoList) {
        this.detallePedidoList = detallePedidoList;
    }

    public List<Movimiento> getMovimientoList() {
        return movimientoList;
    }

    public void setMovimientoList(List<Movimiento> movimientoList) {
        this.movimientoList = movimientoList;
    }

    public Cliente getClienteId() {
        return clienteId;
    }

    public void setClienteId(Cliente clienteId) {
        this.clienteId = clienteId;
    }

    public Mesa getMesaId() {
        return mesaId;
    }

    public void setMesaId(Mesa mesaId) {
        this.mesaId = mesaId;
    }

    public Usuario getUsuarioCancelaId() {
        return usuarioCancelaId;
    }

    public void setUsuarioCancelaId(Usuario usuarioCancelaId) {
        this.usuarioCancelaId = usuarioCancelaId;
    }

    public Usuario getUsuarioDespachaId() {
        return usuarioDespachaId;
    }

    public void setUsuarioDespachaId(Usuario usuarioDespachaId) {
        this.usuarioDespachaId = usuarioDespachaId;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pedidoId != null ? pedidoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pedido)) {
            return false;
        }
        Pedido other = (Pedido) object;
        if ((this.pedidoId == null && other.pedidoId != null) || (this.pedidoId != null && !this.pedidoId.equals(other.pedidoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.Pedido[ pedidoId=" + pedidoId + " ]";
    }
    
}
