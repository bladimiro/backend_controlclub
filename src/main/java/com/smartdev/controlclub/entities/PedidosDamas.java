/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bladimir
 */
@Entity
@Table(name = "pedidos_damas", catalog = "controlclub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "PedidosDamas.findAll", query = "SELECT p FROM PedidosDamas p")})
public class PedidosDamas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pedido_dama_id", nullable = false)
    private Integer pedidoDamaId;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "estado_id")
    private Integer estadoId;
    @JoinColumn(name = "dama_id", referencedColumnName = "dama_id")
    @ManyToOne
    private Damas damaId;
    @JoinColumn(name = "detalle_pedido_id", referencedColumnName = "detalle_pedido_id")
    @ManyToOne
    private DetallePedido detallePedidoId;

    public PedidosDamas() {
    }

    public PedidosDamas(Integer pedidoDamaId) {
        this.pedidoDamaId = pedidoDamaId;
    }

    public Integer getPedidoDamaId() {
        return pedidoDamaId;
    }

    public void setPedidoDamaId(Integer pedidoDamaId) {
        this.pedidoDamaId = pedidoDamaId;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public Damas getDamaId() {
        return damaId;
    }

    public void setDamaId(Damas damaId) {
        this.damaId = damaId;
    }

    public DetallePedido getDetallePedidoId() {
        return detallePedidoId;
    }

    public void setDetallePedidoId(DetallePedido detallePedidoId) {
        this.detallePedidoId = detallePedidoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pedidoDamaId != null ? pedidoDamaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidosDamas)) {
            return false;
        }
        PedidosDamas other = (PedidosDamas) object;
        if ((this.pedidoDamaId == null && other.pedidoDamaId != null) || (this.pedidoDamaId != null && !this.pedidoDamaId.equals(other.pedidoDamaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.PedidosDamas[ pedidoDamaId=" + pedidoDamaId + " ]";
    }   
}