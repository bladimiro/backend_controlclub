/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bladimir
 */
@Entity
@Table(name = "detalle_inventario", catalog = "controlclub", schema = "public")
public class DetalleInventario implements Serializable {
 
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "detalle_inventario_id", nullable = false)
    private Integer detalleInventarioId;
    @Column(name = "cantidad_disponible")
    private Double cantidadDisponible;
    @Column(name = "cantidad_usada")
    private Double cantidadUsada;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "estado_id")
    private Integer estadoId;
    @JoinColumn(name = "inventario_id", referencedColumnName = "inventario_id")
    @ManyToOne
    private Inventario inventarioId;
    @JoinColumn(name = "producto_id", referencedColumnName = "producto_id")
    @ManyToOne
    private Producto productoId;
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id")
    @ManyToOne
    private Usuario usuarioId;

    public DetalleInventario() {
    }

    public DetalleInventario(Integer detalleInventarioId) {
        this.detalleInventarioId = detalleInventarioId;
    }

    public Integer getDetalleInventarioId() {
        return detalleInventarioId;
    }

    public void setDetalleInventarioId(Integer detalleInventarioId) {
        this.detalleInventarioId = detalleInventarioId;
    }

    public Double getCantidadDisponible() {
        return cantidadDisponible;
    }

    public void setCantidadDisponible(Double cantidadDisponible) {
        this.cantidadDisponible = cantidadDisponible;
    }

    public Double getCantidadUsada() {
        return cantidadUsada;
    }

    public void setCantidadUsada(Double cantidadUsada) {
        this.cantidadUsada = cantidadUsada;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public Inventario getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(Inventario inventarioId) {
        this.inventarioId = inventarioId;
    }

    public Producto getProductoId() {
        return productoId;
    }

    public void setProductoId(Producto productoId) {
        this.productoId = productoId;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleInventarioId != null ? detalleInventarioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleInventario)) {
            return false;
        }
        DetalleInventario other = (DetalleInventario) object;
        if ((this.detalleInventarioId == null && other.detalleInventarioId != null) || (this.detalleInventarioId != null && !this.detalleInventarioId.equals(other.detalleInventarioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.DetalleInventario[ detalleInventarioId=" + detalleInventarioId + " ]";
    }
}
