/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bladimir
 */
@Entity
@Table(name = "detalle_comandas", catalog = "controlclub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "DetalleComandas.findAll", query = "SELECT d FROM DetalleComandas d")})
public class DetalleComandas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "detalle_comanda_id", nullable = false)
    private Integer detalleComandaId;
    @Column(name = "estado_registro_id")
    private Integer estadoRegistroId;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "estado_id")
    private Integer estadoId;
    @JoinColumn(name = "comanda_mesero_id", referencedColumnName = "comanda_mesero_id")
    @ManyToOne
    private ComandasMeseros comandaMeseroId;
    @JoinColumn(name = "pedido_id", referencedColumnName = "pedido_id")
    @ManyToOne
    private Pedido pedidoId;

    public DetalleComandas() {
    }

    public DetalleComandas(Integer detalleComandaId) {
        this.detalleComandaId = detalleComandaId;
    }

    public Integer getDetalleComandaId() {
        return detalleComandaId;
    }

    public void setDetalleComandaId(Integer detalleComandaId) {
        this.detalleComandaId = detalleComandaId;
    }

    public Integer getEstadoRegistroId() {
        return estadoRegistroId;
    }

    public void setEstadoRegistroId(Integer estadoRegistroId) {
        this.estadoRegistroId = estadoRegistroId;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public ComandasMeseros getComandaMeseroId() {
        return comandaMeseroId;
    }

    public void setComandaMeseroId(ComandasMeseros comandaMeseroId) {
        this.comandaMeseroId = comandaMeseroId;
    }

    public Pedido getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Pedido pedidoId) {
        this.pedidoId = pedidoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleComandaId != null ? detalleComandaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleComandas)) {
            return false;
        }
        DetalleComandas other = (DetalleComandas) object;
        if ((this.detalleComandaId == null && other.detalleComandaId != null) || (this.detalleComandaId != null && !this.detalleComandaId.equals(other.detalleComandaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.DetalleComandas[ detalleComandaId=" + detalleComandaId + " ]";
    }
    
}
