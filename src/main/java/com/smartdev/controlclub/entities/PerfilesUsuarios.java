/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bladimir
 */
@Entity
@Table(name = "perfiles_usuarios", catalog = "controlclub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "PerfilesUsuarios.findAll", query = "SELECT p FROM PerfilesUsuarios p")})
public class PerfilesUsuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "perfil_usuario_id", nullable = false)
    private Integer perfilUsuarioId;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "estado_id")
    private Integer estadoId;
    @JoinColumn(name = "perfil_id", referencedColumnName = "perfil_id")
    @ManyToOne
    private Perfiles perfilId;
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id")
    @ManyToOne
    private Usuario usuarioId;

    public PerfilesUsuarios() {
    }

    public PerfilesUsuarios(Integer perfilUsuarioId) {
        this.perfilUsuarioId = perfilUsuarioId;
    }

    public Integer getPerfilUsuarioId() {
        return perfilUsuarioId;
    }

    public void setPerfilUsuarioId(Integer perfilUsuarioId) {
        this.perfilUsuarioId = perfilUsuarioId;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public Perfiles getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(Perfiles perfilId) {
        this.perfilId = perfilId;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perfilUsuarioId != null ? perfilUsuarioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PerfilesUsuarios)) {
            return false;
        }
        PerfilesUsuarios other = (PerfilesUsuarios) object;
        if ((this.perfilUsuarioId == null && other.perfilUsuarioId != null) || (this.perfilUsuarioId != null && !this.perfilUsuarioId.equals(other.perfilUsuarioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.PerfilesUsuarios[ perfilUsuarioId=" + perfilUsuarioId + " ]";
    }
    
}
