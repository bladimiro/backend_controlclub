/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author bladimir
 */
@Entity
@Table(name = "comandas_meseros", catalog = "controlclub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "ComandasMeseros.findAll", query = "SELECT c FROM ComandasMeseros c")})
public class ComandasMeseros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "comanda_mesero_id", nullable = false)
    private Integer comandaMeseroId;
    @Column(name = "estado_registro_id")
    private Integer estadoRegistroId;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "fecha_cierre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCierre;
    @Column(name = "estado_id")
    private Integer estadoId;
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id")
    @ManyToOne
    private Usuario usuarioId;
    @Transient
    @OneToMany(mappedBy = "comandaMeseroId")
    private List<DetalleComandas> detalleComandasList;

    public ComandasMeseros() {
    }

    public ComandasMeseros(Integer comandaMeseroId) {
        this.comandaMeseroId = comandaMeseroId;
    }

    public Integer getComandaMeseroId() {
        return comandaMeseroId;
    }

    public void setComandaMeseroId(Integer comandaMeseroId) {
        this.comandaMeseroId = comandaMeseroId;
    }

    public Integer getEstadoRegistroId() {
        return estadoRegistroId;
    }

    public void setEstadoRegistroId(Integer estadoRegistroId) {
        this.estadoRegistroId = estadoRegistroId;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    public List<DetalleComandas> getDetalleComandasList() {
        return detalleComandasList;
    }

    public void setDetalleComandasList(List<DetalleComandas> detalleComandasList) {
        this.detalleComandasList = detalleComandasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comandaMeseroId != null ? comandaMeseroId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComandasMeseros)) {
            return false;
        }
        ComandasMeseros other = (ComandasMeseros) object;
        if ((this.comandaMeseroId == null && other.comandaMeseroId != null) || (this.comandaMeseroId != null && !this.comandaMeseroId.equals(other.comandaMeseroId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.ComandasMeseros[ comandaMeseroId=" + comandaMeseroId + " ]";
    }
    
}
