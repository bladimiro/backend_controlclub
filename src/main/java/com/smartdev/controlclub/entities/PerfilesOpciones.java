/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bladimir
 */
@Entity
@Table(name = "perfiles_opciones", catalog = "controlclub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "PerfilesOpciones.findAll", query = "SELECT p FROM PerfilesOpciones p")})
public class PerfilesOpciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "perfil_opcion_id", nullable = false)
    private Integer perfilOpcionId;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "estado_id")
    private Integer estadoId;
    @JoinColumn(name = "opcion_id", referencedColumnName = "opcion_id")
    @ManyToOne
    private Opciones opcionId;
    @JoinColumn(name = "perfil_id", referencedColumnName = "perfil_id")
    @ManyToOne
    private Perfiles perfilId;

    public PerfilesOpciones() {
    }

    public PerfilesOpciones(Integer perfilOpcionId) {
        this.perfilOpcionId = perfilOpcionId;
    }

    public Integer getPerfilOpcionId() {
        return perfilOpcionId;
    }

    public void setPerfilOpcionId(Integer perfilOpcionId) {
        this.perfilOpcionId = perfilOpcionId;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Integer estadoId) {
        this.estadoId = estadoId;
    }

    public Opciones getOpcionId() {
        return opcionId;
    }

    public void setOpcionId(Opciones opcionId) {
        this.opcionId = opcionId;
    }

    public Perfiles getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(Perfiles perfilId) {
        this.perfilId = perfilId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perfilOpcionId != null ? perfilOpcionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PerfilesOpciones)) {
            return false;
        }
        PerfilesOpciones other = (PerfilesOpciones) object;
        if ((this.perfilOpcionId == null && other.perfilOpcionId != null) || (this.perfilOpcionId != null && !this.perfilOpcionId.equals(other.perfilOpcionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.smartdev.controlclub.entities.PerfilesOpciones[ perfilOpcionId=" + perfilOpcionId + " ]";
    }
    
}
