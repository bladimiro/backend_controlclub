/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Gasto;
import com.smartdev.controlclub.entities.Movimiento;
import com.smartdev.controlclub.model.JsonGasto;
import com.smartdev.controlclub.model.JsonParametros;
import com.smartdev.controlclub.services.GastosServices;
import com.smartdev.controlclub.services.MovimientoServices;
import com.smartdev.controlclub.services.TiposGastosServices;
import com.smartdev.controlclub.services.UsuarioServices;
import com.smartdev.controlclub.utils.ConstEstados;
import com.smartdev.controlclub.utils.ConstValores;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/gasto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GastoController {
    
    @Autowired
    private GastosServices gastoService;
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @Autowired
    private TiposGastosServices tipoGastoService;
    
    @Autowired
    private MovimientoServices movimientoService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        List<JsonGasto> gastosList = gastoService.findAllActivos();
        return new ResponseEntity<>(gastosList, HttpStatus.OK);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonGasto pGasto) throws ParseException{
        Gasto gasto;
        Movimiento movimiento;
        if(pGasto.getGastoId() == null){
            gasto = new Gasto();
            gasto.setFechaRegistro(new Date());
            gasto.setEstadoId(ConstEstados.ACTIVO);
            
            movimiento = new Movimiento();
            movimiento.setTipoId(ConstValores.EGRESO);
            movimiento.setFechaRegistro(new Date());
            movimiento.setEstadoId(ConstEstados.ACTIVO);
        } else {
            gasto = gastoService.findById(pGasto.getGastoId()).get();
            movimiento = movimientoService.findByGastoId(pGasto.getGastoId()).get();
        }
            
        gasto.setDescripcion(pGasto.getDescripcion());
        gasto.setMonto(pGasto.getMonto());
        gasto.setFechaGasto(pGasto.getFechaGasto());
        gasto.setUsuarioId(usuarioService.findById(pGasto.getUsuarioId()).get());
        gasto.setTipoGastoId(tipoGastoService.findById(pGasto.getTipoGastoId()).get());
        gastoService.insertOrUpdate(gasto);
        
        movimiento.setFechaMovimiento(pGasto.getFechaGasto());
        movimiento.setMonto(pGasto.getMonto());
        movimiento.setGlosa(pGasto.getDescripcion());
        movimiento.setGastoId(gasto);
        movimientoService.insertOrUpdate(movimiento);
        return new ResponseEntity<>(gasto, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{gastoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("gastoId") Integer gastoId){
        Optional<Gasto> gasto = gastoService.findById(gastoId);
        if(gasto.isPresent()) {
            JsonGasto jgasto = new JsonGasto();
            jgasto.setGastoId(gasto.get().getGastoId());
            jgasto.setTipoGastoId(gasto.get().getTipoGastoId().getTipoGastoId());
            jgasto.setTipoGastoDesc(gasto.get().getTipoGastoId().getDescripcion());
            jgasto.setFechaGasto(gasto.get().getFechaGasto());
            jgasto.setMonto(gasto.get().getMonto());
            jgasto.setDescripcion(gasto.get().getDescripcion());
            return new ResponseEntity<>(jgasto, HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/deleteById/{gastoId}")
    public ResponseEntity<?> deleteById(@PathVariable("gastoId") Integer gastoId){
        gastoService.deleteById(gastoId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/findByFecha", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByFecha(@RequestBody JsonParametros parametro){
        List<JsonGasto> gastoList = gastoService.findByFecha(parametro.getFechaInicio());
        return new ResponseEntity<>(gastoList, HttpStatus.OK);
    }
}
