/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Pedido;
import com.smartdev.controlclub.model.JsonParametros;
import com.smartdev.controlclub.model.JsonPedidos;
import com.smartdev.controlclub.model.JsonPedidosRespuesta;
import com.smartdev.controlclub.model.JsonRespuestaPedido;
import com.smartdev.controlclub.services.PedidoServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Pedido")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PedidoController {
    
    @Autowired
    private PedidoServices pedidoService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        List<Pedido> pedidoList = pedidoService.findAllActives();
        return new ResponseEntity<>(pedidoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{pedidoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("pedidoId") Integer pedidoId){
        Optional<Pedido> pedido = pedidoService.findById(pedidoId);
        if(pedido.isPresent()){
            JsonPedidos jpedido = new JsonPedidos();
            jpedido.setPedidoId(pedido.get().getPedidoId());
            jpedido.setNumeroPedido(pedido.get().getNumeroPedido());
            jpedido.setMontoTotal(pedido.get().getMontoTotal());
            jpedido.setNombreCliente(pedido.get().getClienteId().getNombre());
            return new ResponseEntity<>(jpedido, HttpStatus.OK);
        }else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody Pedido pPedido){
        Pedido pedido;
        if(pPedido.getPedidoId() == null){
            pedido = new Pedido();
            pedido.setFechaRegistro(new Date());
            pedido.setEstadoId(ConstEstados.ACTIVO);
        } else 
            pedido = pedidoService.findById(pPedido.getPedidoId()).get();
        pedidoService.insertOrUpdate(pedido);
        return new ResponseEntity<>(pedido, HttpStatus.OK);
    }
    
    @PostMapping(path = "/enviarPedido", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> enviarPedido(@RequestBody JsonPedidos pPedidos){   
        JsonRespuestaPedido respuesta = pedidoService.enviarPedido(pPedidos);
        return new ResponseEntity<>(respuesta, HttpStatus.OK);
    }
    
    @PostMapping(path = "/consolidarPedido", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> consolidarPedido(@RequestBody JsonPedidos pPedidos){   
        pedidoService.consolidarPedido(pPedidos);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/enviarPedidoVasoChica", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> enviarPedidoVasoChica(@RequestBody JsonPedidos pPedidos){   
        JsonRespuestaPedido respuesta = pedidoService.enviarPedidoVasoChica(pPedidos);
        return new ResponseEntity<>(respuesta, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{pedidoId}")
    public ResponseEntity<?> delete(@PathVariable("pedidoId") Integer pedidoId){
        Pedido pedido = pedidoService.findById(pedidoId).get();
        pedidoService.delete(pedido);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping(path = "/findByEstadoPedidoId/{estadoPedidoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByEstadoPedidoId(@PathVariable("estadoPedidoId") Integer estadoPedidoId){ 
        List<JsonPedidosRespuesta> pedidosList = pedidoService.findByEstadoPedidoId(estadoPedidoId);
        return new ResponseEntity<>(pedidosList, HttpStatus.OK);
    }
    
    @PostMapping(path = "/findByEstadoPedidoIdFechas", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByEstadoPedidoIdFechas(@RequestBody JsonParametros parametros){
        List<JsonPedidosRespuesta> pedidosList = pedidoService.findByEstadoPedidoIdFechas(parametros.getId(),
                                                 parametros.getFechaInicio(), parametros.getFechaFin());
        return new ResponseEntity<>(pedidosList, HttpStatus.OK);
    }
    
    @PostMapping(path = "/cancelarPedido")
    public ResponseEntity<?> cancelarPedido(@RequestBody JsonPedidos pPedidos){
        Optional<Pedido> pedido = pedidoService.findById(pPedidos.getPedidoId());
        if(pedido.isPresent()){
            pedidoService.cancelarPedido(pPedidos);
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/despacharPedido")
    public ResponseEntity<?> despacharPedido(@RequestBody JsonPedidos pPedidos){
        Optional<Pedido> pedido = pedidoService.findById(pPedidos.getPedidoId());
        if(pedido.isPresent()){
            pedidoService.atenderPedido(pPedidos);
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/findByTipoPagoId/{tipoPagoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByTipoPagoId(@PathVariable("tipoPagoId") Integer tipoPagoId){
        List<JsonPedidosRespuesta> pedidoList = pedidoService.findByTipoPagoId(tipoPagoId);
        return new ResponseEntity<>(pedidoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findByUsuarioIdEstadoPedidoId/{usuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByUsuarioIdEstadoPedidoId(@PathVariable("usuarioId") Integer usuarioId){
        Optional<Pedido> pedido = pedidoService.findByUsuarioIdEstadoPedidoId(usuarioId);
        if(pedido.isPresent()){
            JsonPedidos jpedido = new JsonPedidos();
            jpedido.setPedidoId(pedido.get().getPedidoId());
            jpedido.setNumeroPedido(pedido.get().getNumeroPedido());
            jpedido.setMontoTotal(pedido.get().getMontoTotal());
            jpedido.setNombreCliente(pedido.get().getClienteId().getNombre());
            return new ResponseEntity<>(jpedido, HttpStatus.OK);
        }else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    /// Opciones de guardado
    @PostMapping(path = "/guardarPedidoVasoChica", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> guardarPedidoVasoChica(@RequestBody JsonPedidos pPedidos){
        pedidoService.guardarPedidoVasoChica(pPedidos);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/agregarPedidoVasoChica", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> agregarPedidoVasoChica(@RequestBody JsonPedidos pPedidos){
        pedidoService.agregarPedidoVasoChica(pPedidos);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/guardarPedido", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> guardarPedido(@RequestBody JsonPedidos jpedido) {
        pedidoService.guardarPedido(jpedido);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/agregarPedido", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> agregarPedido(@RequestBody JsonPedidos jpedido){
        pedidoService.agregarPedido(jpedido);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/pagarPedido", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> pagarPedido(@RequestBody JsonPedidos jpedido){
        pedidoService.pagarPedido(jpedido);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
