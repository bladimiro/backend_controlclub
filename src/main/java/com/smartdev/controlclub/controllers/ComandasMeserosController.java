/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.ComandasMeseros;
import com.smartdev.controlclub.model.JsonComandasMeseros;
import com.smartdev.controlclub.model.JsonParametros;
import com.smartdev.controlclub.services.ComandasMeserosServices;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
//import javax.print.
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/ComandasMeseros")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ComandasMeserosController {
    
    @Autowired
    private ComandasMeserosServices comandaMeseroService;
    
    @GetMapping(path = "/findAbiertoByUsuarioId/{usuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAbiertoByUsuarioId(@PathVariable("usuarioId") Integer usuarioId){
        Optional<ComandasMeseros> comandaMesero = comandaMeseroService.findAbiertoByUsuarioId(usuarioId);
        if(comandaMesero.isPresent())
            return new ResponseEntity<>(comandaMesero.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/cerrarComandas/{usuarioId}")
    public ResponseEntity<?> cerrarComandas(@PathVariable("usuarioId") Integer usuarioId){
        comandaMeseroService.cerrarComandaMesero(usuarioId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping(path = "/findCerradoByEstadoRegistroId/{usuarioId}/{estadoRegistroId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findCerradoByEstadoRegistroId(@PathVariable("usuarioId") Integer usuarioId, 
                        @PathVariable("estadoRegistroId") Integer estadoRegistroId){
        List<ComandasMeseros> comandasMesero = comandaMeseroService.findCerradoByEstadoRegistroId(usuarioId, estadoRegistroId);
        return new ResponseEntity<>(comandasMesero, HttpStatus.OK);
    }
    
    @PostMapping(path = "/findByEstadoRegistroIdFechas", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByEstadoRegistroIdFechas(@RequestBody JsonParametros parametros){
        List<ComandasMeseros> comandasList = comandaMeseroService.findByEstadoRegistroIdFechas(parametros.getId(), 
                        parametros.getFechaInicio(), parametros.getFechaFin());
        return new ResponseEntity<>(comandasList, HttpStatus.OK);
    }
    
    @PostMapping(path = "/insertOrUpdate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonParametros parametro){
        
        return new ResponseEntity<>( HttpStatus.OK);
    }
    
    @PostMapping(path = "/obtenerTotalesComandasByFecha", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> obtenerTotalesComandasByFecha(@RequestBody JsonParametros parametros){
        List<JsonComandasMeseros> comandasList = comandaMeseroService.obtenerTotalesComandasByFecha(parametros.getFechaInicio());
        return new ResponseEntity<>(comandasList, HttpStatus.OK);
    }
    
    @PostMapping(path = "/obtenerTotal", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> obtenerTotal(@RequestBody JsonParametros parametro){
        List<JsonComandasMeseros> comandasList = comandaMeseroService.obtenerTotalesComandasByFecha(parametro.getFechaInicio());
        Double total = comandasList.stream().mapToDouble(p -> p.getTotal()).sum();
        System.out.println(total);
        return new ResponseEntity<>(total, HttpStatus.OK);
    }
}
