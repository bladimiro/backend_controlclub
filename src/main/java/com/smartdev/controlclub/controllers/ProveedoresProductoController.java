/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.ProveedorProducto;
import com.smartdev.controlclub.model.JsonProveedoresProducto;
import com.smartdev.controlclub.services.ProductosServices;
import com.smartdev.controlclub.services.ProveedoresProductoServices;
import com.smartdev.controlclub.services.ProveedoresServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/ProveedoresProducto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProveedoresProductoController {
    
    @Autowired
    private ProveedoresProductoServices provProductoService;
    
    @Autowired
    private ProductosServices productoService;
    
    @Autowired
    private ProveedoresServices proveedorService;
    
    @GetMapping(path = "/findByProductoId/{productoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByProductoId(@PathVariable("productoId") Integer productoId){
        List<JsonProveedoresProducto> productosList = provProductoService.findByProductoId(productoId);
        return new ResponseEntity<>(productosList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findByActives", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByActives(){
        List<JsonProveedoresProducto> provProductoList = provProductoService.findByActive();
        return new ResponseEntity<>(provProductoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{provProductoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("provProductoId") Integer provProductoId){
        Optional<ProveedorProducto> provProducto = provProductoService.findById(provProductoId);
        if(provProducto.isPresent())
            return new ResponseEntity<>(provProducto.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonProveedoresProducto jprovProducto){
        ProveedorProducto proveedorProd;
        if(jprovProducto.getProvProductoId() == null){
            proveedorProd = new ProveedorProducto();
            proveedorProd.setProductoId(productoService.findById(jprovProducto.getProductoId()).get());
            proveedorProd.setFechaRegistro(new Date());
            proveedorProd.setEstadoId(ConstEstados.ACTIVO);
        } else
            proveedorProd = provProductoService.findById(jprovProducto.getProvProductoId()).get();
        
        proveedorProd.setProveedorId(proveedorService.findById(jprovProducto.getProveedorId()).get());
        proveedorProd.setCantidad(jprovProducto.getCantidad());
        proveedorProd.setFechaCompra(jprovProducto.getFechaCompra());
        proveedorProd.setPrecio(jprovProducto.getPrecio());
        provProductoService.insertOrUpdate(proveedorProd);
        return new ResponseEntity<>(proveedorProd, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{provProductoId}")
    public ResponseEntity<?> delete(@PathVariable("provProductoId") Integer provProductoId){
        Optional<ProveedorProducto> provProducto = provProductoService.findById(provProductoId);
        if(provProducto.isPresent()){
            provProductoService.delete(provProducto.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
