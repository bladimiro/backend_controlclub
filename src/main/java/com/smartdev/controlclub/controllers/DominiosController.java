/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Dominios;
import com.smartdev.controlclub.services.DominiosServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Dominio")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DominiosController {
    
    @Autowired
    private DominiosServices dominioService;
    
    @GetMapping(path = "/findByDominio/{dominio}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByDominio(@PathVariable("dominio") String dominio){
        List<Dominios> dominioList = dominioService.findByDominio(dominio);
        return new ResponseEntity<>(dominioList, HttpStatus.OK);
    }
}
