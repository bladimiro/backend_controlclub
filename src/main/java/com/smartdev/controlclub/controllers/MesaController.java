/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Mesa;
import com.smartdev.controlclub.model.JsonMesa;
import com.smartdev.controlclub.services.MesasServices;
import com.smartdev.controlclub.services.SalasServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Mesa")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MesaController {
    
    @Autowired
    private MesasServices mesaService;
    
    @Autowired
    private SalasServices salaService;
    
    @GetMapping(path = "/findBySalaId/{salaId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findBySalaId(@PathVariable("salaId") Integer salaId ){
        List<JsonMesa> mesas = mesaService.findBySalaId(salaId);
        return new ResponseEntity<>(mesas, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findAllActives", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAllActives(){
        List<JsonMesa> mesas = mesaService.findAllActives();
        return new ResponseEntity<>(mesas, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{mesaId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("mesaId") Integer mesaId){
        Optional<Mesa> mesa = mesaService.findById(mesaId);
        if(mesa.isPresent())
            return new ResponseEntity<>(mesa.get(), HttpStatus.OK);
        else 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonMesa pMesa){
        Mesa mesa;
        if(pMesa.getMesaId() == null){
            mesa = new Mesa();
            mesa.setSalaId(salaService.findById(pMesa.getSalaId()).get());
            mesa.setFechaRegistro(new Date());
            mesa.setEstadoId(ConstEstados.ACTIVO);
        } else 
            mesa = mesaService.findById(pMesa.getMesaId()).get();
        mesa.setNumero(pMesa.getNumero());
        mesaService.insertOrUpdate(mesa);
        return new ResponseEntity<>(mesa, HttpStatus.OK);
    }
    
    @GetMapping(path = "/deleteById/{mesaId}")
    public ResponseEntity deleteById(@PathVariable("mesaId") Integer mesaId){
        Optional<Mesa> mesa = mesaService.findById(mesaId);
        if(mesa.isPresent()){
            mesaService.delete(mesa.get());
            return new ResponseEntity(HttpStatus.OK);
        } else
            return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
