/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Producto;
import com.smartdev.controlclub.model.JsonProducto;
import com.smartdev.controlclub.services.DetalleInventarioServices;
import com.smartdev.controlclub.services.InventariosServices;
import com.smartdev.controlclub.services.ProductosServices;
import com.smartdev.controlclub.services.UsuarioServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Productos")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductoController {
    
    @Autowired
    private ProductosServices productoService;
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @Autowired
    private InventariosServices inventarioService;
    
    @Autowired
    private DetalleInventarioServices detalleInventarioService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        List<Producto> productoList = productoService.findAllActives();
        return new ResponseEntity<>(productoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{productoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("productoId") Integer productoId){
        Optional<Producto> producto = productoService.findById(productoId);
        if(producto.isPresent())
            return new ResponseEntity<>(producto.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonProducto pProduc){
        Producto producto;
        if(pProduc.getProductoId() == null){
            producto = new Producto();
            producto.setFechaRegistro(new Date());
            producto.setEstadoId(ConstEstados.ACTIVO);
            producto.setUsuarioId(usuarioService.findById(pProduc.getUsuarioId()).get());
        } else 
            producto = productoService.findById(pProduc.getProductoId()).get();
        producto.setNombre(pProduc.getNombre());
        producto.setPrecio(pProduc.getPrecio());
        producto.setCantidadVasos(pProduc.getCantidadVasos());
        productoService.insertOrUpdate(producto);
        return new ResponseEntity<>(producto, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{productoId}")
    public ResponseEntity<?> delete(@PathVariable("productoId") Integer productoId){
        Producto producto = productoService.findById(productoId).get();
        productoService.delete(producto);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping(path = "/existeRelacion/{productoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> existeRelacion(@PathVariable("productoId") Integer productoId){
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
