/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.TipoGasto;
import com.smartdev.controlclub.services.TiposGastosServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/tipoGasto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TipoGastoController {
    
    @Autowired
    private TiposGastosServices tipoGastoService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        List<TipoGasto> tipoGastoList = tipoGastoService.findAllActivos();
        return new ResponseEntity<>(tipoGastoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{tipoGastoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("tipoGastoId") Integer tipoGastoId){
        Optional<TipoGasto> tipoGasto = tipoGastoService.findById(tipoGastoId);
        if(tipoGasto.isPresent())
            return new ResponseEntity<>(tipoGasto.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody TipoGasto tGasto){
        TipoGasto tipoGasto;
        if(tGasto.getTipoGastoId() == null){
            tipoGasto = new TipoGasto();
            tipoGasto.setFechaRegistro(new Date());
            tipoGasto.setEstadoId(ConstEstados.ACTIVO);
        } else
            tipoGasto = tipoGastoService.findById(tGasto.getTipoGastoId()).get();
        tipoGasto.setDescripcion(tGasto.getDescripcion());
        tipoGastoService.insertOrUpdate(tipoGasto);
        return new ResponseEntity(tipoGasto, HttpStatus.OK);
    }
    
    @GetMapping(path = "/deleteById/{tipoGastoId}")
    public ResponseEntity<?> deleteById(@PathVariable("tipoGastoId") Integer tipoGastoId){
        tipoGastoService.deleteById(tipoGastoId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
