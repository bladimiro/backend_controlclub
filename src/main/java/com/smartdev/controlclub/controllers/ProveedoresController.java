/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Proveedor;
import com.smartdev.controlclub.services.PrinterService;
import com.smartdev.controlclub.services.ProveedoresServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Proveedores")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProveedoresController {
    
    @Autowired
    private ProveedoresServices proveedorService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        //PrinterService printerService = new PrinterService();
        List<Proveedor> proveedorList = proveedorService.findAllActives();
        //printerService.printString("EPSON TM-T20II Receipt", "\n\n testing testing 4 5 6");
        return new ResponseEntity<>(proveedorList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{proveedorId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("proveedorId") Integer proveedorId){
        Optional<Proveedor> proveedor = proveedorService.findById(proveedorId);
        if(proveedor.isPresent())
            return new ResponseEntity<>(proveedor.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody Proveedor prov){
        Proveedor proveedor;
        if(prov.getProveedorId() == null){
            proveedor = new Proveedor();
            proveedor.setFechaRegistro(new Date());
            proveedor.setEstadoId(ConstEstados.ACTIVO);
        } else 
            proveedor = proveedorService.findById(prov.getProveedorId()).get();
        proveedor.setNombre(prov.getNombre());
        proveedor.setTelefono1(prov.getTelefono1());
        proveedor.setTelefono2(prov.getTelefono2());
        proveedor.setEmail(prov.getEmail());
        proveedor.setDireccion(prov.getDireccion());
        proveedorService.insertOrUpdate(proveedor);
        return new ResponseEntity<>(proveedor, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{proveedorId}")
    public ResponseEntity<?> delete(@PathVariable("proveedorId") Integer proveedorId){
        Proveedor proveedor = proveedorService.findById(proveedorId).get();
        proveedorService.delete(proveedor);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
