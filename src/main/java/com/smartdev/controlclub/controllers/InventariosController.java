/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Inventario;
import com.smartdev.controlclub.model.JsonInventario;
import com.smartdev.controlclub.model.JsonRespuestaGeneral;
import com.smartdev.controlclub.services.InventariosServices;
import com.smartdev.controlclub.services.UsuarioServices;
import com.smartdev.controlclub.utils.ConstEstadoInventario;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Inventarios")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class InventariosController {
    
    @Autowired
    private InventariosServices inventarioService;
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @GetMapping(path = "/verificarRegistrarInventarioAbierto/{usuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByEstadoInventario(@PathVariable("usuarioId") Integer usuarioId){
        Optional<Inventario> inventario = inventarioService.findByEstadoInventario(ConstEstadoInventario.ABIERTO);
        Inventario inventarioResp;
        if(inventario.isPresent()){
            inventarioResp = inventario.get();
        } else {
            inventarioResp = new Inventario();
            inventarioResp.setFechaInicio(new Date());
            inventarioResp.setEstadoId(ConstEstados.ACTIVO);
            inventarioResp.setEstadoInventarioId(ConstEstadoInventario.ABIERTO);
            inventarioResp.setFechaRegistro(new Date());
            inventarioResp.setUsuarioId(usuarioService.findById(usuarioId).get());
            inventarioService.insertOrUpdate(inventarioResp);
        }
        return new ResponseEntity<>(inventarioResp, HttpStatus.OK);
    }
    
    @PostMapping(path = "/insertOrUpdate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonInventario jinventario){
        Inventario inventario;
        if(jinventario.getInventarioId() == null){
            inventario = new Inventario();
            inventario.setFechaRegistro(new Date());
            inventario.setEstadoId(ConstEstados.ACTIVO);
            inventario.setEstadoInventarioId(ConstEstadoInventario.ABIERTO);
        } else 
            inventario = inventarioService.findById(jinventario.getInventarioId()).get();
        inventario.setUsuarioId(usuarioService.findById(jinventario.getUsuarioId()).get());
        inventario.setFechaInicio(jinventario.getFechaInicio());
        inventarioService.insertOrUpdate(inventario);
        return new ResponseEntity<>(inventario, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{inventarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("inventarioId") Integer inventarioId){
        Optional<Inventario> inventario = inventarioService.findById(inventarioId);
        if(inventario.isPresent())
            return new ResponseEntity<>(inventario.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/findByEstadoHistorico", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByEstadoHistorico(){
        List<JsonInventario> inventarioList = inventarioService.findByEstadoHistorico();
        return new ResponseEntity<>(inventarioList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/cerrarInventario/{inventarioId}/{usuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> cerrarInventario(@PathVariable("inventarioId") Integer inventarioId, 
                            @PathVariable("usuarioId") Integer usuarioId){
        //System.out.println("cerrando");
        
        Optional<Inventario> inventarioOpt = inventarioService.findById(inventarioId);
        if(inventarioOpt.isPresent()){
            JsonRespuestaGeneral respuesta = new JsonRespuestaGeneral();
            
            Inventario inventario = inventarioOpt.get();
            if(inventario.getEstadoInventarioId().equals(ConstEstadoInventario.ABIERTO)) {
                respuesta.setCodigo("0");
                inventarioService.cerrarInventario(inventario, usuarioId);
            } else {
                respuesta.setCodigo("1");
                respuesta.setDescripcion("El Inventario ya se encuentra cerrado");
            }
            
            return new ResponseEntity<>(respuesta, HttpStatus.OK);
        } else 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
    }
    
    @GetMapping(path = "/findDetalleById/{inventarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findDetalleById(@PathVariable("inventarioId") Integer inventarioId) {
        JsonInventario inventario = inventarioService.findDetalleById(inventarioId);
        return new ResponseEntity<>(inventario, HttpStatus.OK);
    }
}
