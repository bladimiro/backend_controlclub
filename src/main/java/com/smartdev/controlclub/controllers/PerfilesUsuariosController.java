/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.PerfilesUsuarios;
import com.smartdev.controlclub.model.JsonOpciones;
import com.smartdev.controlclub.model.JsonPerfilesUsuarios;
import com.smartdev.controlclub.services.PerfilesServices;
import com.smartdev.controlclub.services.PerfilesUsuariosServices;
import com.smartdev.controlclub.services.UsuarioServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/PerfilesUsuarios")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PerfilesUsuariosController {
    
    @Autowired
    private PerfilesUsuariosServices perfUsuarioService;
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @Autowired
    private PerfilesServices perfilService;
    
    @GetMapping(path = "/findOpcionesPrincipalesByUsuarioId/{usuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findOpcionesPrincipalesByUsuarioId(@PathVariable("usuarioId") Integer usuarioId){
        List<JsonOpciones> opcionesList = perfUsuarioService.findOpcionesPrincipalesByUsuarioId(usuarioId);
        return new ResponseEntity<>(opcionesList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{perfilUsuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("perfilUsuarioId") Integer perfilUsuarioId){
        Optional<PerfilesUsuarios> perfUsuario = perfUsuarioService.findById(perfilUsuarioId);
        if(perfUsuario.isPresent())
            return new ResponseEntity<>(perfUsuario.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonPerfilesUsuarios jperfilUser){
        PerfilesUsuarios perfilUsuario;
        if(jperfilUser.getPerfilUsuarioId() == null){
            perfilUsuario = new PerfilesUsuarios();
            perfilUsuario.setUsuarioId(usuarioService.findById(jperfilUser.getUsuarioId()).get());
            perfilUsuario.setFechaRegistro(new Date());
            perfilUsuario.setEstadoId(ConstEstados.ACTIVO);
        } else
            perfilUsuario = perfUsuarioService.findById(jperfilUser.getPerfilUsuarioId()).get();
        perfilUsuario.setPerfilId(perfilService.findById(jperfilUser.getPerfilId()).get());
        perfUsuarioService.insertOrUpdate(perfilUsuario);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{perfilUserId}")
    public ResponseEntity<?> delete(@PathVariable("perfilUserId") Integer perfilUserId){
        Optional<PerfilesUsuarios> perfUsuario = perfUsuarioService.findById(perfilUserId);
        if(perfUsuario.isPresent()){
            perfUsuarioService.delete(perfUsuario.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/findPerfilesByUsuarioId/{usuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findPerfilesByUsuarioId(@PathVariable("usuarioId") Integer usuarioId){
        List<JsonPerfilesUsuarios> perfilesList = perfUsuarioService.findPerfilesByUsuarioId(usuarioId);
        return new ResponseEntity<>(perfilesList, HttpStatus.OK);
    }
}
