/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Cliente;
import com.smartdev.controlclub.services.ClienteServices;
import com.smartdev.controlclub.services.PrinterService;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Clientes")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ClienteController {
    
    @Autowired
    private ClienteServices clienteService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        //PrinterService printerService = new PrinterService();
        //
        List<Cliente> clientesList = clienteService.findByActives();
        //printerService.printString("HP LaserJet Professional P1102w", "\n\n testing testing 1 2 3eeeee ");
        //printerService.printString("EPSON-TM-T20II", "\n\n testing testing 1 2 3");
        //System.out.println("print");
        return new ResponseEntity<>(clientesList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{clienteId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("clienteId") Integer clienteId) {
        Optional<Cliente> cliente = clienteService.findById(clienteId);
        if(cliente.isPresent())
            return new ResponseEntity<>(cliente.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody Cliente pCliente){
        Cliente cliente;
        if(pCliente.getClienteId() == null){
            cliente = new Cliente();
            cliente.setFechaRegistro(new Date());
            cliente.setEstadoId(ConstEstados.ACTIVO);
        } else
            cliente = clienteService.findById(pCliente.getClienteId()).get();
        cliente.setNombre(pCliente.getNombre());
        cliente.setNit(pCliente.getNit());
        clienteService.insertOrUpdate(cliente);
        return new ResponseEntity<>(cliente, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{clienteId}")
    public ResponseEntity<?> delete(@PathVariable("clienteId") Integer clienteId){
        Optional<Cliente> cliente = clienteService.findById(clienteId);
        if(cliente.isPresent()) {
            clienteService.delete(cliente.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/findByDocumentoIdentidad/{documento}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByDocumentoIdentidad(@PathVariable("documento") String documento){
        System.out.println(documento);
        Optional<Cliente> cliente = clienteService.findByDocumentoIdentidad(documento);
        if(cliente.isPresent())
            return new ResponseEntity<>(cliente.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
