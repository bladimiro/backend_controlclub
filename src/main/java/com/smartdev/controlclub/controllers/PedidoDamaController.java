/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.model.JsonParametros;
import com.smartdev.controlclub.model.JsonPedidoDama;
import com.smartdev.controlclub.services.PedidosDamasServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/PedidoDama")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PedidoDamaController {
    
    @Autowired
    private PedidosDamasServices pedidoDamaService;
    
    @PostMapping(path = "/findByDamaIdFecha", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    private ResponseEntity<?> findByDamaIdFecha(@RequestBody JsonParametros parametro ){
        List<JsonPedidoDama> pedidosList = pedidoDamaService.findByDamaIdFecha(parametro.getId(), 
                    parametro.getFechaInicio(), parametro.getFechaFin());
        return new ResponseEntity<>(pedidosList, HttpStatus.OK);
    }
}
