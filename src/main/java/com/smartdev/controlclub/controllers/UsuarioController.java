/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Usuario;
import com.smartdev.controlclub.model.JsonUsuario;
import com.smartdev.controlclub.services.UsuarioServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/usuario")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UsuarioController {
    
    @Autowired
    private UsuarioServices usuarioService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        List<Usuario> usuarioList = usuarioService.findAllActivos();
        return new ResponseEntity<>(usuarioList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{usuarioId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("usuarioId") Integer usuarioId){
        Optional<Usuario> usuario = usuarioService.findById(usuarioId);
        if(usuario.isPresent())
            return new ResponseEntity<>(usuario.get(), HttpStatus.OK);
        else 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody Usuario pUser){
        Usuario usuario;
        if(pUser.getUsuarioId() == null){
            usuario = new Usuario();
            usuario.setFechaRegistro(new Date());
            usuario.setEstadoId(ConstEstados.ACTIVO);
            usuario.setHabilitado(true);
        } else
            usuario = usuarioService.findById(pUser.getUsuarioId()).get();
        usuario.setApellidos(pUser.getApellidos());
        usuario.setNombres(pUser.getNombres());
        usuario.setUsername(pUser.getUsername());
        usuario.setPassword(pUser.getPassword());
        usuarioService.insertOrUpdate(usuario);
        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }
    
    @GetMapping(path = "/cambiarEstado/{usuarioId}/{estado}")
    public ResponseEntity<?> cambiarEstado(@PathVariable("usuarioId") Integer usuarioId, @PathVariable("estado") boolean estado){
        usuarioService.cambiarEstado(usuarioId, estado);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping(path = "/deleteById/{usuarioId}")
    public ResponseEntity<?> deleteById(@PathVariable("usuarioId") Integer usuarioId){
        usuarioService.deleteById(usuarioId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/verifyLogin", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> verifyLogin(@RequestBody JsonUsuario pUser) {
        
        Optional<JsonUsuario> usuario = usuarioService.verifyByLogin(pUser);
        if(usuario.isPresent()){
            System.out.println("found");
            return new ResponseEntity<>(usuario.get(), HttpStatus.OK);
        }else{
            System.out.println("not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
