/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.DetallePedido;
import com.smartdev.controlclub.model.JsonDetallePedido;
import com.smartdev.controlclub.services.DetallePedidoServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/DetallePedido")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DetallePedidoController {
    
    @Autowired
    private DetallePedidoServices detallePedidoService;
    
    @GetMapping(path = "/findByPedidoId/{pedidoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByPedidoId(@PathVariable("pedidoId") Integer pedidoId){
        List<JsonDetallePedido> detalleList = detallePedidoService.findByPedidoId(pedidoId);
        return new ResponseEntity<>(detalleList, HttpStatus.OK);
    }
}
