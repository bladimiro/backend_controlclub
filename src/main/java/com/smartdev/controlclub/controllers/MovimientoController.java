/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Movimiento;
import com.smartdev.controlclub.model.JsonMovimiento;
import com.smartdev.controlclub.model.JsonParametros;
import com.smartdev.controlclub.services.GastosServices;
import com.smartdev.controlclub.services.MovimientoServices;
import com.smartdev.controlclub.services.PedidoServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Movimiento")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MovimientoController {
    
    @Autowired
    private MovimientoServices movimientoService;
    
    @Autowired
    private GastosServices gastoService;
    
    @Autowired
    private PedidoServices pedidoService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        List<JsonMovimiento> movimientoList = movimientoService.findByActives();
        return new ResponseEntity<>(movimientoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{movimientoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("movimientoId") Integer movimientoId){
        Optional<Movimiento> movimiento = movimientoService.findById(movimientoId);
        if(movimiento.isPresent())
            return new ResponseEntity<>(movimiento.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/findByFechas", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByFechas(@RequestBody JsonParametros parametros){
        List<JsonMovimiento> movimientos = movimientoService.findByFechas(parametros.getFechaInicio(), parametros.getFechaFin());
        return new ResponseEntity<>(movimientos, HttpStatus.OK);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonMovimiento jmovimiento){
        Movimiento movimiento;
        if(jmovimiento.getMovimientoId() == null){
            movimiento = new Movimiento();
            movimiento.setFechaRegistro(new Date());
            movimiento.setEstadoId(ConstEstados.ACTIVO);
        } else
            movimiento = movimientoService.findById(jmovimiento.getMovimientoId()).get();
        movimiento.setFechaMovimiento(jmovimiento.getFechaMovimiento());
        movimiento.setMonto(jmovimiento.getMonto());
        movimiento.setGlosa(jmovimiento.getGlosa());
        if(jmovimiento.getGastoId() != null)
            movimiento.setGastoId(gastoService.findById(jmovimiento.getGastoId()).get());
        if(jmovimiento.getPedidoId() != null)
            movimiento.setPedidoId(pedidoService.findById(jmovimiento.getPedidoId()).get());
        movimientoService.insertOrUpdate(movimiento);
        return new ResponseEntity<>(movimiento, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{movimientoId}")
    public ResponseEntity<?> delete(@PathVariable("movimientoId") Integer movimientoId){
        Optional<Movimiento> movimiento = movimientoService.findById(movimientoId);
        if(movimiento.isPresent()){
            movimientoService.delete(movimiento.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
