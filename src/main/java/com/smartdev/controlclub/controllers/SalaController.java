/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Sala;
import com.smartdev.controlclub.model.JsonSala;
import com.smartdev.controlclub.services.EmpresaServices;
import com.smartdev.controlclub.services.SalasServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Sala")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalaController {
    
    @Autowired
    private SalasServices salaService;
    
    @Autowired
    private EmpresaServices empresaService;
    
    @GetMapping(path = "/findByEmpresaId/{empresaId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByEmpresaId(@PathVariable("empresaId") Integer empresaId){
        List<JsonSala> salasList = salaService.findByEmpresaId(empresaId);
        return new ResponseEntity<>(salasList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{salaId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("salaId") Integer salaId){
        Optional<Sala> sala = salaService.findById(salaId);
        if(sala.isPresent())
            return new ResponseEntity<>(sala.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonSala pSala){
        Sala sala;
        if(pSala.getSalaId() == null){
            sala = new Sala();
            sala.setFechaRegistro(new Date());
            sala.setEstadoId(ConstEstados.ACTIVO);
            sala.setEmpresaId(empresaService.findById(pSala.getEmpresaId()).get());
        } else
            sala = salaService.findById(pSala.getSalaId()).get();
        sala.setNombreSala(pSala.getNombreSala());
        sala.setTurno(pSala.getTurno());
        salaService.insertOrUpdate(sala);
        return new ResponseEntity<>(sala, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{salaId}")
    public ResponseEntity<?> delete(@PathVariable("salaId") Integer salaId){
        Optional<Sala> sala = salaService.findById(salaId);
        if(sala.isPresent()){
            salaService.delete(sala.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
