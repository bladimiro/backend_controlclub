/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Damas;
import com.smartdev.controlclub.services.DamasServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Damas")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DamasController {
    
    @Autowired 
    private DamasServices damaService;
    
    @GetMapping(path = "/findByActives", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByActives(){
        List<Damas> damaList = damaService.findByActives();
        return new ResponseEntity<>(damaList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findByEstado/{estado}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByEstado(@PathVariable("estado") boolean estado){
        List<Damas> damaList = damaService.findByHabilitado(estado);
        return new ResponseEntity<>(damaList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{damaId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("damaId") Integer damaId ){
        Optional<Damas> dama = damaService.findById(damaId);
        return new ResponseEntity<>(dama.get(), HttpStatus.OK);
    }
    
    @GetMapping(path = "/cambiarEstado/{damaId}/{estado}")
    public ResponseEntity<?> cambiarEstado(@PathVariable("damaId") Integer damaId, @PathVariable("estado") boolean estado){
        damaService.cambiarEstado(damaId, estado);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody Damas pDama){
        Damas dama;
        if(pDama.getDamaId() == null){
            dama = new Damas();
            dama.setFechaRegistro(new Date());
            dama.setEstadoId(ConstEstados.ACTIVO);
            dama.setHabilitada(true);
        } else
            dama = damaService.findById(pDama.getDamaId()).get();
        dama.setCodigo(pDama.getCodigo());
        dama.setNombre(pDama.getNombre());
        damaService.insertOrUpdate(dama);
        return new ResponseEntity<>(dama, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findByCodigo/{codigo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByCodigo(@PathVariable("codigo") String codigo){
        Optional<Damas> dama = damaService.findByCodigo(codigo);
        if(dama.isPresent())
            return new ResponseEntity<>(dama.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
