/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Empresa;
import com.smartdev.controlclub.services.EmpresaServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/empresa")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EmpresaController {
    
    @Autowired
    private EmpresaServices empresaService;
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody Empresa emp){
        Empresa empresa;
        if(emp.getEmpresaId() == null){
            empresa = new Empresa();
            empresa.setFechaRegistro(new Date());
            empresa.setEstadoId(ConstEstados.ACTIVO);
        } else {
            empresa = empresaService.findById(emp.getEmpresaId()).get();
        }
        empresa.setNombre(emp.getNombre());
        empresa.setDireccion(emp.getDireccion());
        empresa.setTelefono1(emp.getTelefono1());
        empresa.setTelefono2(emp.getTelefono2());
        empresa.setCorreoElectronico(emp.getCorreoElectronico());
        empresaService.insertOrUpdate(empresa);
        return new ResponseEntity<>(empresa, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{empresaId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("empresaId") Integer empresaId){
        Optional<Empresa> empresa = empresaService.findById(empresaId);
        if(empresa.isPresent())
            return new ResponseEntity<>(empresa.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/findFirst", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findFirst(){
        Empresa empresa = empresaService.findFirst();
        return new ResponseEntity<>(empresa, HttpStatus.OK);
    }
}
