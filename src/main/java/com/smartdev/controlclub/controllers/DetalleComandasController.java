/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.DetalleComandas;
import com.smartdev.controlclub.model.JsonDetalleComanda;
import com.smartdev.controlclub.model.JsonParametros;
import com.smartdev.controlclub.services.ComandasMeserosServices;
import com.smartdev.controlclub.services.DetalleComandasServices;
import com.smartdev.controlclub.services.PedidoServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/DetalleComandas")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DetalleComandasController {
    
    @Autowired
    private DetalleComandasServices detalleComandaService;
    
    @Autowired
    private ComandasMeserosServices comandaMeseroService;
    
    @Autowired
    private PedidoServices pedidoService;
    
    @GetMapping(path = "/findByComandaMeseroId/{comandaMeseroId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByComandaMeseroId(@PathVariable("comandaMeseroId") Integer comandaMeseroId){
        List<JsonDetalleComanda> detalleList = detalleComandaService.findByComandaMeseroId(comandaMeseroId);
        return new ResponseEntity<>(detalleList, HttpStatus.OK);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonDetalleComanda pDetalle){
        DetalleComandas detalleComanda;
        if(pDetalle.getDetalleComandaId() == null){
            detalleComanda = new DetalleComandas();
            detalleComanda.setFechaRegistro(new Date());
            detalleComanda.setEstadoId(ConstEstados.ACTIVO);
            detalleComanda.setComandaMeseroId(comandaMeseroService.findById(pDetalle.getComandaMeseroId()).get());
        } else 
            detalleComanda = detalleComandaService.findById(pDetalle.getDetalleComandaId()).get();
        detalleComanda.setPedidoId(pedidoService.findById(pDetalle.getPedidoId()).get());
        detalleComandaService.insertOrUpdate(detalleComanda);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("id") Integer id){
        Optional<DetalleComandas> detalle = detalleComandaService.findById(id);
        if(detalle.isPresent())
            return new ResponseEntity<>(detalle.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/findByUsuarioIdFecha", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByUsuarioIdFecha(@RequestBody JsonParametros parametros){
        List<JsonDetalleComanda> detalleList = detalleComandaService.findByUsuarioIdFecha(
                parametros.getId(), parametros.getFechaInicio());
        return new ResponseEntity<>(detalleList, HttpStatus.OK);
    }
}
