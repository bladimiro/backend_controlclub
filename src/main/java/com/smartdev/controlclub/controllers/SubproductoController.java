/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartdev.controlclub.controllers;

import com.smartdev.controlclub.entities.Subproducto;
import com.smartdev.controlclub.model.JsonSubproducto;
import com.smartdev.controlclub.services.ProductosServices;
import com.smartdev.controlclub.services.SubproductoServices;
import com.smartdev.controlclub.utils.ConstEstados;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bladimir
 */
@RestController
@RequestMapping("/Subproducto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SubproductoController {
    
    @Autowired
    private SubproductoServices subproductoService;
    
    @Autowired
    private ProductosServices productoService;
    
    @GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(){
        List<JsonSubproducto> subproductoList = subproductoService.findAllActives();
        return new ResponseEntity<>(subproductoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findByProductoId/{productoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByProductoId(@PathVariable("productoId") Integer productoId){
        List<JsonSubproducto> subproductoList = subproductoService.findByProductoId(productoId);
        return new ResponseEntity<>(subproductoList, HttpStatus.OK);
    }
    
    @GetMapping(path = "/findById/{subprodId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("subprodId") Integer subprodId){
        Optional<Subproducto> subproducto = subproductoService.findById(subprodId);
        if(subproducto.isPresent()){
            JsonSubproducto subprod = new JsonSubproducto();
            subprod.setSubproductoId(subproducto.get().getSubproductoId());
            subprod.setProductoId(subproducto.get().getProductoId().getProductoId());
            subprod.setPrecio(subproducto.get().getPrecio());
            subprod.setNombre(subproducto.get().getNombre());
            subprod.setEquivalencia(subproducto.get().getEquivalencia());
            subprod.setCantidadChicas(subproducto.get().getCantidadChicas());
            subprod.setComision(subproducto.get().getComision());
            subprod.setTipoSolicitudId(subproducto.get().getTipoSolicitudId());
            subprod.setTipoId(subproducto.get().getTipoId());
            return new ResponseEntity<>(subprod, HttpStatus.OK);
        }else 
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @PostMapping(path = "/insertOrUpdate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insertOrUpdate(@RequestBody JsonSubproducto psubprud){
        Subproducto subprod;
        if(psubprud.getSubproductoId() == null){
            subprod = new Subproducto();
            subprod.setFechaRegistro(new Date());
            subprod.setProductoId(productoService.findById(psubprud.getProductoId()).get());
            subprod.setEstadoId(ConstEstados.ACTIVO);
        } else
            subprod = subproductoService.findById(psubprud.getSubproductoId()).get();
        subprod.setTipoId(psubprud.getTipoId());
        subprod.setEquivalencia(psubprud.getEquivalencia());
        subprod.setNombre(psubprud.getNombre());
        subprod.setPrecio(psubprud.getPrecio());
        subprod.setCantidadChicas(psubprud.getCantidadChicas());
        subprod.setTipoSolicitudId(psubprud.getTipoSolicitudId());
        subprod.setComision(psubprud.getComision());
        subproductoService.insertOrUpdate(subprod);
        return new ResponseEntity<>(subprod, HttpStatus.OK);
    }
    
    @GetMapping(path = "/delete/{subproductoId}")
    public ResponseEntity<?> delete(@PathVariable("subproductoId") Integer subproductoId){
        Optional<Subproducto> subprod = subproductoService.findById(subproductoId);
        if(subprod.isPresent()){
            subproductoService.delete(subprod.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(path = "/findByTipoIdTiSolicitante/{tipoId}/{tipoSolicitudId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByTipoIdTiSolicitante(@PathVariable("tipoId") Integer tipoId, 
                                    @PathVariable("tipoSolicitudId") Integer tipoSolicitudId) {
        List<JsonSubproducto> subproductoList = subproductoService.findByTipoIdTiSolicitante(tipoId, tipoSolicitudId);
        return new ResponseEntity<>(subproductoList, HttpStatus.OK);
    }
}
